// Bai3.cpp : Defines the entry point for the console application.
//

#include <math.h>
#include <iostream>
#include "supportClass.h"
#include "Mesh.h"
#include <tchar.h>
using namespace std;

int		screenWidth = 600;
int		screenHeight= 300;

Mesh	tetrahedron;
Mesh	cube;
Mesh	cuboi;
Mesh	tru;
Mesh	truver2;
Mesh	ovan;
Mesh	bannguyet;

int		nChoice = 1;

void drawAxis()
{
	glColor3f(0, 0, 1);
	glBegin(GL_LINES);
		glVertex3f(0, 0, 0);
		glVertex3f(4, 0, 0);

		glVertex3f(0, 0, 0);
		glVertex3f(0, 4, 0);

		glVertex3f(0, 0, 0);
		glVertex3f(0, 0, 4);
	glEnd();
}
void myDisplay()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(4.5, 4, 2, 0, 0, 0, 0, 1, 0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	
	glViewport(0, 0, screenWidth/2, screenHeight);
	
	drawAxis();

	glColor3f(0, 0, 0);
	if(nChoice == 1)
		tetrahedron.DrawWireframe();
	else if(nChoice == 2)
		cube.DrawWireframe();
	else if(nChoice == 3)
		cuboi.DrawWireframe();
	else if(nChoice == 4)
		tru.DrawWireframe();
	else if(nChoice == 5)
		truver2.DrawWireframe();
	else if(nChoice == 6)
		ovan.DrawWireframe();
	else if(nChoice == 7)
		bannguyet.DrawWireframe();


	glViewport(screenWidth/2, 0, screenWidth/2, screenHeight);

	drawAxis();
	if(nChoice == 1)
		tetrahedron.DrawColor();
	else if(nChoice == 2)
		cube.DrawColor();
	else if(nChoice == 3)
		cuboi.DrawColor();
	else if(nChoice == 4)
		tru.DrawColor();
	else if(nChoice == 5)
		truver2.DrawColor();
	else if(nChoice == 6)
		ovan.DrawColor();
	else if(nChoice == 7)
		bannguyet.DrawColor();

	glFlush();
    glutSwapBuffers();
}

void myInit()
{
	float	fHalfSize = 4;

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	glFrontFace(GL_CCW);
	glEnable(GL_DEPTH_TEST);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-fHalfSize, fHalfSize, -fHalfSize, fHalfSize, -1000, 1000);
}

int _tmain(int argc, _TCHAR* argv[])
{
	cout << "1. Tetrahedron" << endl;
	cout << "2. Cube" << endl;
	cout << "3. CubeId" << endl;
	cout << "4. Hinh tru" << endl;
	cout << "5. Bien the hinh tru" << endl;
	cout << "6. Hinh ovan co lo" << endl;
	cout << "7. Hinh ban nguyet" << endl;
	cout << "Input the choice: " << endl;
	cin  >> nChoice;

	glutInit(&argc, (char**)argv); //initialize the tool kit
	glutInitDisplayMode(GLUT_DOUBLE |GLUT_RGB | GLUT_DEPTH);//set the display mode
	glutInitWindowSize(screenWidth, screenHeight); //set window size
	glutInitWindowPosition(100, 100); // set window position on screen
	glutCreateWindow("Lab 2"); // open the screen window

	tetrahedron.CreateTetrahedron();
	cube.CreateCube(1);
	cuboi.CreateCuboid(0.5,1.5,0.7);
	tru.CreateTR(1,2);
	truver2.CreateTruVer2(1.5, 1.7, 1, 1);
	ovan.CreateOvan(0.5, 4, 1, 1.5);
	bannguyet.CreateBanNguyet(2, 1, 0.8, 4);


	myInit();
    glutDisplayFunc(myDisplay);
	  
	glutMainLoop();
	return 0;
}

