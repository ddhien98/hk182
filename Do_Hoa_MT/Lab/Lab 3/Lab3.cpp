// Bai3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <math.h>
#include <iostream>
#include "supportClass.h"
#include "Mesh.h"

using namespace std;

#define PI			3.1415926

int		screenWidth = 600;
int		screenHeight= 600;

bool	bWireFrame = false;

float	base1Radius = 0.8;
float	base1Height = 0.2;
float	base1RotateStep = 5;

float	base2Radius = 0.6;
float	base2Height = 1.2;

float	cylinderRadius = 0.4;
float	cylinderHeight = 1.6;
float	cylinderRotateStep = 5;
float	cylinderTranslationStep = 0.05;
//float	cylinderOffset = base2Height-cylinderHeight/2;
float	cylinderOffset = 0;

Mesh	base1;
Mesh	base2;
Mesh	cylinder;

void myKeyboard(unsigned char key, int x, int y)
{
	float	fRInc;
	float	fAngle;
    switch(key)
    {
	case '1':
		base1.rotateY += base1RotateStep;
		if(base1.rotateY > 360)
			base1.rotateY -=360;
		break;
	case '2':	
		base1.rotateY -= base1RotateStep;
		if(base1.rotateY < 0)
			base1.rotateY +=360;
		break;
	case '3':
		cylinder.rotateY += cylinderRotateStep;
		if(cylinder.rotateY > 360)
			cylinder.rotateY -=360;
		break;
	case '4':	
		cylinder.rotateY -= cylinderRotateStep;
		if(cylinder.rotateY < 0)
			cylinder.rotateY +=360;
		break;
	case '5':
		cylinderOffset += cylinderTranslationStep;
		if(cylinderOffset > base2Height/2)
			cylinderOffset = base2Height/2;
		break;
	case '6':	
		cylinderOffset -= cylinderTranslationStep;
		if(cylinderOffset<0)
			cylinderOffset=0;
		break;
	
	case 'w':
	case 'W':
		bWireFrame = !bWireFrame;
		break;
	}
    glutPostRedisplay();
}
void drawAxis()
{
	glColor3f(0, 0, 1);
	glBegin(GL_LINES);
		glColor3f(1, 0, 0);
		glVertex3f(0, 0, 0);//x
		glVertex3f(4, 0, 0);

		glColor3f(0, 1, 0);
		glVertex3f(0, 0, 0);//y
		glVertex3f(0, 4, 0);

		glColor3f(0, 0, 1);
		glVertex3f(0, 0, 0);//z
		glVertex3f(0, 0, 4);
	glEnd();
}

void drawBase1()
{
	glPushMatrix();

		glTranslated(0, base1Height/2.0, 0);
		glRotatef(base1.rotateY, 0, 1, 0);

		if(bWireFrame)
			base1.DrawWireframe();
		else
			base1.DrawColor();

	glPopMatrix();
}

void drawBase2()
{
	glPushMatrix();

		glTranslated(0, base2Height/2.0+base1Height, 0);
		glRotatef(base1.rotateY, 0, 1, 0);

		if(bWireFrame)
			base2.DrawWireframe();
		else
			base2.DrawColor();

	glPopMatrix();
}

void drawCylinder()
{
	glPushMatrix();

		glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		glRotatef(cylinder.rotateY+base1.rotateY , 0, 1, 0);

		if(bWireFrame)
			cylinder.DrawWireframe();
		else
			cylinder.DrawColor();

	glPopMatrix();
}

void myDisplay()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(6, 4, 6, 0, 1, 0, 0, 1, 0);
	

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	
	glViewport(0, 0, screenWidth, screenHeight);
	
	drawAxis();

	drawBase1();
	drawBase2();
	drawCylinder();
	
	glFlush();
    glutSwapBuffers();
}

void myInit()
{
	float	fHalfSize = 4;

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	glFrontFace(GL_CCW);
	glEnable(GL_DEPTH_TEST);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-fHalfSize, fHalfSize, -fHalfSize, fHalfSize, -1000, 1000);
}

int _tmain(int argc, _TCHAR* argv[])
{
	glutInit(&argc, (char**)argv); //initialize the tool kit
	glutInitDisplayMode(GLUT_DOUBLE |GLUT_RGB | GLUT_DEPTH);//set the display mode
	glutInitWindowSize(screenWidth, screenHeight); //set window size
	glutInitWindowPosition(100, 100); // set window position on screen
	glutCreateWindow("Lab 3 - Demo (2018-2019)"); // open the screen window

	base1.CreateCylinder(20, base1Height, base1Radius);
	base1.SetColor(2);

	base2.CreateCylinder(20, base2Height, base2Radius);
	base2.SetColor(2);

	cylinder.CreateCylinder(20, cylinderHeight, cylinderRadius);
	cylinder.SetColor(3);

	myInit();

	glutKeyboardFunc(myKeyboard);
    glutDisplayFunc(myDisplay);
	  
	glutMainLoop();
	return 0;
}

