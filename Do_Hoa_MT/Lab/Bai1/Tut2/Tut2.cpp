#include <windows.h>
#include <glut.h>
#include <math.h>
#include <iostream>
#define PI 3.1415926



using namespace std;

int screenWidth = 500;
int screenHeight = 500;

void init()
{
    

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
 //   //glOrtho(0, screenWidth, 0, screenHeight, -1, 1);
 //   glOrtho(0, screenWidth, screenHeight, 0, -1, 1);
	//glClearColor(1, 1, 1, 1);
 //   glColor3f(1, 0, 0);
	gluOrtho2D(-2.0, 2.0, -2.0, 2.0);	
    glMatrixMode(GL_MODELVIEW);
    glClearColor(1.0, 1.0, 1.0,1.0);
    glColor3f(0.0,0.0,0.0);

}


void setCameraVolume(float l, float r, float b, float t){
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D((GLdouble)l,(GLdouble)r,(GLdouble)b,(GLdouble)t);
}

void setViewport(int l, int r, int b, int t){
	glViewport(l, b, r - l, t - b);
}

// CHuc nang cua ham la: Ve hai duong vong cung doi xung nhau.

void drawArcs(){
	glColor3f(1, 0, 0);
	glLineWidth(1);
	float R = 0.5;
	
	setViewport(0, 580, 0, 580);
	glBegin(GL_LINE_STRIP);
	for (int i = 0; i <= 90; i++){
		float x = R*cos(i*PI / 180.0);
		float y = R*sin(i*PI / 180.0);
		glVertex2f(x, y);
	}
	glEnd();
	glBegin(GL_LINE_STRIP);
	for (int i = 180; i <= 270; i++){
		float x = R*cos(i*PI / 180.0) + 1;
		float y = R*sin(i*PI / 180.0) + 1;
		glVertex2f(x, y);
	}
	glEnd();
}


void mydisplay(){
	glClear(GL_COLOR_BUFFER_BIT);
	drawArcs();
	glFlush();
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(600, 600);
    glutInitWindowPosition(0, 0);
    glutCreateWindow("Mouse Move - Depress mouse button (left or right)");

    glutDisplayFunc(mydisplay);
    //glutMotionFunc(drawSquare);
 //   //init();
	setCameraVolume(-2, 2, -2, 2);
	

    glutMainLoop();
}

