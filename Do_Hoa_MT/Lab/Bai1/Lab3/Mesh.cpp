
#include "Mesh.h"
#include <math.h>
#include <iostream>

#define PI			3.1415926
#define	COLORNUM		15


float	ColorArr[COLORNUM][3] = {{0.9, 0.0, 0.0}, {0.0, 0.9, 0.0}, { 0.0,  0.0, 0.9}, 
								{0.9, 0.7,  0.0}, { 1.0, 0.0, 1.0},{ 0.0, 1.0, 1.0}, 
								 {0.3, 0.3, 0.3}, {0.4, 0.4, 0.4}, { 0.9,  0.9, 0.9},
								{1.0, 0.5,  0.5}, { 0.2, 0.6, 0.8},{ 0.0, 0.8, 0.9},
								{0.0, 0.0, 0.0}, {1.0, 1.0, 1.0}, {0.7, 0.0, 0.0}};


int Mesh::SetColor(int colorIdx){
	for (int f = 0; f < numFaces; f++){
		for (int v = 0; v < face[f].nVerts; v++){
			face[f].vert[v].colorIndex = colorIdx;
		}
	}
	return 0;
}

void Mesh::Draw() {
	for (int f = 0; f < numFaces; f++){
		glBegin(GL_POLYGON);
		for (int v = 0; v < face[f].nVerts; v++){
			int		iv = face[f].vert[v].vertIndex;
			glNormal3f(face[f].facenorm.x, face[f].facenorm.y, face[f].facenorm.z);
			glVertex3f(pt[iv].x, pt[iv].y, pt[iv].z);
		}
		glEnd();
	}
}

void Mesh::CalculateFacesNorm(){
	for (int i = 0; i < numFaces; i++){
		for (int j = 0; j < face[i].nVerts - 1; j++){
			face[i].facenorm.x += (pt[face[i].vert[j].vertIndex].y-pt[face[i].vert[j+1].vertIndex].y)*(pt[face[i].vert[j].vertIndex].z+pt[face[i].vert[j+1].vertIndex].z); 
			face[i].facenorm.y += (pt[face[i].vert[j].vertIndex].z-pt[face[i].vert[j+1].vertIndex].z)*(pt[face[i].vert[j].vertIndex].x+pt[face[i].vert[j+1].vertIndex].x); 
			face[i].facenorm.z += (pt[face[i].vert[j].vertIndex].x-pt[face[i].vert[j+1].vertIndex].x)*(pt[face[i].vert[j].vertIndex].y+pt[face[i].vert[j+1].vertIndex].y); 
		
		}
	}

}

// OK
void Mesh::CreateDeGia(float khoangcach)
{
	int i;

	numVerts=8;
	pt = new Point3[numVerts];
	// z la 
	float x, y, z;
	x = 0.5; y = 0.1; z = 0.2;
	pt[0].set( -z + khoangcach, y, x);
	pt[1].set( z + khoangcach, y, x);
	pt[2].set( z + khoangcach, y, -x);
	pt[3].set(-z + khoangcach, y, -x);
	pt[4].set(-z + khoangcach, 0, x);
	pt[5].set( z + khoangcach, 0, x);
	pt[6].set( z + khoangcach, 0, -x);
	pt[7].set(-z + khoangcach, 0, -x);


	numFaces= 6;
	face = new Face[numFaces];

	//Left face
	face[0].nVerts = 4;
	face[0].vert = new VertexID[face[0].nVerts];
	face[0].vert[0].vertIndex = 1;
	face[0].vert[1].vertIndex = 5;
	face[0].vert[2].vertIndex = 6;
	face[0].vert[3].vertIndex = 2;
	
	//Right face
	face[1].nVerts = 4;
	face[1].vert = new VertexID[face[1].nVerts];
	face[1].vert[0].vertIndex = 0;
	face[1].vert[1].vertIndex = 4;
	face[1].vert[2].vertIndex = 7;
	face[1].vert[3].vertIndex = 3;

	//top face
	face[2].nVerts = 4;
	face[2].vert = new VertexID[face[2].nVerts];
	face[2].vert[0].vertIndex = 0;
	face[2].vert[1].vertIndex = 1;
	face[2].vert[2].vertIndex = 2;
	face[2].vert[3].vertIndex = 3;

	//bottom face
	face[3].nVerts = 4;
	face[3].vert = new VertexID[face[3].nVerts];
	face[3].vert[0].vertIndex = 7;
	face[3].vert[1].vertIndex = 4;
	face[3].vert[2].vertIndex = 5;
	face[3].vert[3].vertIndex = 6;
	/*for(i = 0; i<face[3].nVerts ; i++)
		face[3].vert[i].colorIndex = 3;*/

	//near face
	/*face[4].nVerts = 4;
	face[4].vert = new VertexID[face[4].nVerts];
	face[4].vert[0].vertIndex = 0;
	face[4].vert[1].vertIndex = 4;
	face[4].vert[2].vertIndex = 5;
	face[4].vert[3].vertIndex = 1;*/
	/*for(i = 0; i<face[4].nVerts ; i++)
		face[4].vert[i].colorIndex = 4;*/

	//Far face
	/*face[5].nVerts = 4;
	face[5].vert = new VertexID[face[5].nVerts];
	face[5].vert[0].vertIndex = 3;
	face[5].vert[1].vertIndex = 7;
	face[5].vert[2].vertIndex = 6;
	face[5].vert[3].vertIndex = 2;*/
	/*for(i = 0; i<face[5].nVerts ; i++)
		face[5].vert[i].colorIndex = 5;*/

}

void Mesh::CreateHinhThoi(float R){
	const int N = 6;
	float x[N], y[N];
	numVerts = N;
	pt = new Point3[numVerts+1];
	pt[0].set(0, 0, 0);
	int theta = -90;
	// Tim toa do cac dinh
	for (int i = 0; i < N; i++){
		x[i] = R*cos(theta*3.14/180);
		y[i] = R*sin(theta*3.14/180);
		theta += 360/N;
	}

	for(int i = 0; i < numVerts;i++){
		pt[i+1].set(x[i], 0, y[i]);
	}

	numFaces = 3;
	face = new Face[numFaces];

	face[0].nVerts = 4;
	face[0].vert = new VertexID[4];
	face[0].vert[0].vertIndex = 0;
	face[0].vert[1].vertIndex = 1;
	face[0].vert[2].vertIndex = 2;
	face[0].vert[3].vertIndex = 3;
	for(int i = 0; i<face[0].nVerts ; i++)
		face[0].vert[i].colorIndex = 13;

	face[1].nVerts = 4;
	face[1].vert = new VertexID[4];
	face[1].vert[0].vertIndex = 0;
	face[1].vert[1].vertIndex = 3;
	face[1].vert[2].vertIndex = 4;
	face[1].vert[3].vertIndex = 5;
	for(int i = 0; i<face[0].nVerts ; i++)
		face[1].vert[i].colorIndex = 11;

	face[2].nVerts = 4;
	face[2].vert = new VertexID[4];
	face[2].vert[0].vertIndex = 0;
	face[2].vert[1].vertIndex = 5;
	face[2].vert[2].vertIndex = 6;
	face[2].vert[3].vertIndex = 1;
	for(int i = 0; i<face[0].nVerts ; i++)
		face[2].vert[i].colorIndex = 10;
}

void Mesh::DrawWireframe()
{
	glColor3f(0, 0, 0);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	for (int f = 0; f < numFaces; f++)
	{
		glBegin(GL_POLYGON);
		for (int v = 0; v < face[f].nVerts; v++)
		{
			int		iv = face[f].vert[v].vertIndex;

			glVertex3f(pt[iv].x, pt[iv].y, pt[iv].z);
		}
		glEnd();
	}
}

void Mesh::DrawColor()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	for (int f = 0; f < numFaces; f++)
	{
		glBegin(GL_POLYGON);
		for (int v = 0; v < face[f].nVerts; v++)
		{
			int		iv = face[f].vert[v].vertIndex;
			int		ic = face[f].vert[v].colorIndex;
			
			//ic = f % COLORNUM;

			glColor3f(ColorArr[ic][0], ColorArr[ic][1], ColorArr[ic][2]); 
			glVertex3f(pt[iv].x, pt[iv].y, pt[iv].z);
		}
		glEnd();
	}
}

void Mesh::CreateCuboid(float	fSizeX, float fSizeY, float	fSizeZ){
	int i;

	numVerts=8;
	pt = new Point3[numVerts];
	pt[0].set(-fSizeX, fSizeY, fSizeZ);
	pt[1].set( fSizeX, fSizeY, fSizeZ);
	pt[2].set( fSizeX, fSizeY, -fSizeZ);
	pt[3].set(-fSizeX, fSizeY, -fSizeZ);
	pt[4].set(-fSizeX, -fSizeY, fSizeZ);
	pt[5].set( fSizeX, -fSizeY, fSizeZ);
	pt[6].set( fSizeX, -fSizeY, -fSizeZ);
	pt[7].set(-fSizeX, -fSizeY, -fSizeZ);


	numFaces= 6;
	face = new Face[numFaces];

	//Left face
	face[0].nVerts = 4;
	face[0].vert = new VertexID[face[0].nVerts];
	face[0].vert[0].vertIndex = 1;
	face[0].vert[1].vertIndex = 5;
	face[0].vert[2].vertIndex = 6;
	face[0].vert[3].vertIndex = 2;
	
	//Right face
	face[1].nVerts = 4;
	face[1].vert = new VertexID[face[1].nVerts];
	face[1].vert[0].vertIndex = 0;
	face[1].vert[1].vertIndex = 3;
	face[1].vert[2].vertIndex = 7;
	face[1].vert[3].vertIndex = 4;

	//top face
	face[2].nVerts = 4;
	face[2].vert = new VertexID[face[2].nVerts];
	face[2].vert[0].vertIndex = 0;
	face[2].vert[1].vertIndex = 1;
	face[2].vert[2].vertIndex = 2;
	face[2].vert[3].vertIndex = 3;

	//bottom face
	face[3].nVerts = 4;
	face[3].vert = new VertexID[face[3].nVerts];
	face[3].vert[0].vertIndex = 7;
	face[3].vert[1].vertIndex = 6;
	face[3].vert[2].vertIndex = 5;
	face[3].vert[3].vertIndex = 4;

	//near face
	face[4].nVerts = 4;
	face[4].vert = new VertexID[face[4].nVerts];
	face[4].vert[0].vertIndex = 4;
	face[4].vert[1].vertIndex = 5;
	face[4].vert[2].vertIndex = 1;
	face[4].vert[3].vertIndex = 0;

	//Far face
	face[5].nVerts = 4;
	face[5].vert = new VertexID[face[5].nVerts];
	face[5].vert[0].vertIndex = 3;
	face[5].vert[1].vertIndex = 2;
	face[5].vert[2].vertIndex = 6;
	face[5].vert[3].vertIndex = 7;

}

void Mesh::CreateTR(float R,float H){
	const int N = 20;
	float x[N], y[N];
	
	numVerts = N;
	pt = new Point3[numVerts*2 + 2];
	pt[0].set(0,0,0);
	pt[1].set(0,H,0);
	int theta = 0;
	// Tim toa do cac dinh	theo chieu am
	for (int i = 0; i < N; i++){
		x[i] = R*cos(theta*3.14/180);
		y[i] = R*sin(theta*3.14/180);
		theta += 360/N;
	}

	for(int i = 0; i < numVerts;i++){
		pt[i+2].set(x[i], 0, y[i]);
	}

	for(int i = numVerts; i < numVerts*2; i++){
		pt[i+2].set(x[i-numVerts], H, y[i-numVerts]);
	}

	numFaces = N*3;
	face = new Face[numFaces];

	// Draw cicle bottom
	for (int i = 0; i < N; i++){
		face[i].nVerts = 3;
		face[i].vert = new VertexID[face[i].nVerts];
		face[i].vert[0].vertIndex = 0;
		face[i].vert[2].vertIndex = i+2;
		if (i == N-1){
			face[i].vert[1].vertIndex = 2;
		}
		else{
			
			face[i].vert[1].vertIndex = i+3;
		}
		
	}

	// Draw cicle top
	for (int i = N; i < N*2; i++){
		face[i].nVerts = 3;
		face[i].vert = new VertexID[face[i].nVerts];
		face[i].vert[0].vertIndex = 1;
		face[i].vert[1].vertIndex = i+2;
		if (i == N*2-1){
			face[i].vert[2].vertIndex = N+2;
		}
		else{
			face[i].vert[2].vertIndex = i+3;
		}
	}

	// Draw arround
	for (int i = N*2; i < N*3; i++){
		face[i].nVerts = 4;
		face[i].vert = new VertexID[face[i].nVerts];
		face[i].vert[0].vertIndex = i - N*2 + 2;
		face[i].vert[3].vertIndex = i - N + 2;
		if (i == N*3 - 1){
			face[i].vert[1].vertIndex = 2;
			face[i].vert[2].vertIndex = N + 2;
			face[i].vert[0].vertIndex = i - N*2 + 2;
			face[i].vert[3].vertIndex = i - N + 2;
		}
		else{
			face[i].vert[1].vertIndex = i - N*2 + 3;
			face[i].vert[2].vertIndex = i - N + 3;
		}
		
	}
}

void Mesh::CreateTruVer2(float r1, float h1, float r2, float h2, float l){
	const int N = 20;
	// x1 toa do duoi, y1 toa do tren, x2, y2 hinh nho
	float x1[N], y1[N], x2[N], y2[N];
	
	numVerts = N;
	pt = new Point3[numVerts*4 + 2];
	pt[0].set(0,0,0);
	pt[1].set(0,h1+h2+l,0);
	int theta = 0;
	// Tim toa do cac dinh
	for (int i = 0; i < N; i++){
		x1[i] = r1*cos(theta*3.14/180);
		y1[i] = r1*sin(theta*3.14/180);
		theta += 360/N;
	}
	theta = 0;
	for (int i = 0; i < N; i++){
		x2[i] = r2*cos(theta*3.14/180);
		y2[i] = r2*sin(theta*3.14/180);
		theta += 360/N;
	}

	// Set vert
	// Vert bottom
	for(int i = 0; i < numVerts;i++){
		pt[i+2].set(x1[i], 0, y1[i]);
	}
	// Vert middle 1
	for(int i = numVerts; i < numVerts*2; i++){
		pt[i+2].set(x1[i-numVerts], h1, y1[i-numVerts]);
	}
	// Vert middle 2
	for(int i = numVerts*2; i < numVerts*3; i++){
		pt[i+2].set(x2[i-numVerts*2], h1 + l, y2[i-numVerts*2]); // cong them l
	}
	// Vert top
	for(int i = numVerts*3; i < numVerts*4; i++){
		pt[i+2].set(x2[i-numVerts*3], h1+h2 + l, y2[i-numVerts*3]); // cong them l
	}

	
	numFaces = N*5;
	face = new Face[numFaces];
	// Draw cicle bottom
	for (int i = 0; i < N; i++){
		face[i].nVerts = 3;
		face[i].vert = new VertexID[face[i].nVerts];
		face[i].vert[0].vertIndex = 0;
		face[i].vert[1].vertIndex = i+2;
		if (i == N-1){
			face[i].vert[2].vertIndex = 2;
		}
		else{
			face[i].vert[2].vertIndex = i+3;
		}
		
	}
	// Draw middle
	for (int i = N; i < N*2; i++){
		face[i].nVerts = 4;
		face[i].vert = new VertexID[face[i].nVerts];
		face[i].vert[0].vertIndex = i+2;
		face[i].vert[3].vertIndex = i+N+2;
		if (i == N*2-1){
			face[i].vert[1].vertIndex = N+2;
			face[i].vert[2].vertIndex = N*2+2;
		}
		else{
			face[i].vert[1].vertIndex = i+3;
			face[i].vert[2].vertIndex = i+N+3;
		}
	}
	// Draw cicle top
	for (int i = 0; i < N; i++){
		face[i+N*2].nVerts = 3;
		face[i+N*2].vert = new VertexID[face[i+N*2].nVerts];
		face[i+N*2].vert[0].vertIndex = 1;
		face[i+N*2].vert[1].vertIndex = i+N*3+2;
		if (i == N-1){
			face[i+N*2].vert[2].vertIndex = N*3+2;
		}
		else{
			face[i+N*2].vert[2].vertIndex = i+N*3+3;
		}
		
	}
	// Draw arround bottom
	for (int i = 0; i < N; i++){
		face[i+N*3].nVerts = 4;
		face[i+N*3].vert = new VertexID[face[i+N*3].nVerts];
		face[i+N*3].vert[0].vertIndex = i + 2;
		face[i+N*3].vert[3].vertIndex = i + N + 2;
		if (i == N - 1){
			face[i+N*3].vert[1].vertIndex = 3;
			face[i+N*3].vert[2].vertIndex = N + 3;
		}
		else{
			face[i+N*3].vert[1].vertIndex = i + 3;
			face[i+N*3].vert[2].vertIndex = i + N + 3;
		}
		
	}
	// Draw arround top
	for (int i = 0; i < N; i++){
		face[i+N*4].nVerts = 4;
		face[i+N*4].vert = new VertexID[face[i+N*4].nVerts];
		face[i+N*4].vert[0].vertIndex = i + N*2 + 2;
		face[i+N*4].vert[3].vertIndex = i + N*3 + 2;
		if (i == N - 1){
			face[i+N*4].vert[1].vertIndex = 3 + N*2;
			face[i+N*4].vert[2].vertIndex = N*3 + 3;
		}
		else{
			face[i+N*4].vert[1].vertIndex = i + N*2 + 3;
			face[i+N*4].vert[2].vertIndex = i + N*3 + 3;
		}
		
	}
}

void Mesh::CreateOvan(float x, float y, float r, float h){
	const int N = 19;
	// x1 toa do duoi -x, y1 toa do tren -x, x2, y2 nguoc lai
	float x1[N], y1[N], x2[N], y2[N];
	
	numVerts = N;
	pt = new Point3[numVerts*8];
	int theta = 90;
	// Tim toa do cac dinh
	for (int i = 0; i < N; i++){
		x1[i] = r*cos(theta*3.14/180) ;
		y1[i] = r*sin(theta*3.14/180) ;
		theta += 10;
	}
	theta = 90;
	for (int i = 0; i < N; i++){
		x2[i] = (x)*cos(theta*3.14/180);
		y2[i] = (x)*sin(theta*3.14/180);
		theta += 10;
	}

	// Set vert
	// Vert bottom left
	for(int i = 0; i < numVerts;i++){
		pt[i].set(x1[i] - y/2.0, 0, y1[i]);
	}
	for(int i = numVerts; i < numVerts*2;i++){
		pt[i].set(x2[i-numVerts] - y/2.0, 0, y2[i-numVerts]);
	}
	// Vert top left
	for(int i = numVerts*2; i < numVerts*3;i++){
		pt[i].set(x1[i-numVerts*2] - y/2.0, h, y1[i-numVerts*2]);
	}
	for(int i = numVerts*3; i < numVerts*4;i++){
		pt[i].set(x2[i-numVerts*3] - y/2.0, h, y2[i-numVerts*3]);
	}
	// Set Vert Right
	for(int i = numVerts*4; i < numVerts*5;i++){
		pt[i].set(-x1[i-numVerts*4] + y/2.0, 0, y1[i-numVerts*4]);
	}
	for(int i = numVerts*5; i < numVerts*6;i++){
		pt[i].set(-x2[i-numVerts*5] + y/2.0, 0, y2[i-numVerts*5]);
	}
	for(int i = numVerts*6; i < numVerts*7;i++){
		pt[i].set(-x1[i-numVerts*6] + y/2.0, h, y1[i-numVerts*6]);
	}
	for(int i = numVerts*7; i < numVerts*8;i++){
		pt[i].set(-x2[i-numVerts*7] + y/2.0, h, y2[i-numVerts*7]);
	}


	numFaces = N*8;
	face = new Face[numFaces];
	//////////// Draw phia cuc ben trai
	// Draw cicle bottom
	for (int i = 0; i < N - 1; i++){
		// Day trai Xanh duong
		face[i].nVerts = 4;
		face[i].vert = new VertexID[face[i].nVerts];
		face[i].vert[0].vertIndex = i;
		face[i].vert[1].vertIndex = i+1;
		face[i].vert[2].vertIndex = i+N+1;
		face[i].vert[3].vertIndex = i+N;
		
		/*for(int j = 0; j<face[i].nVerts ; j++)
			face[i].vert[j].colorIndex = i;*/
	}
	// Phia tren ben trai
	for (int i = N; i < N*2-1;i++){
		// Phia tren ben trai
		face[i - 1].nVerts = 4;
		face[i - 1].vert = new VertexID[face[i - 1].nVerts];
		face[i - 1].vert[0].vertIndex = i+N;
		face[i - 1].vert[1].vertIndex = i+N+1;
		face[i - 1].vert[2].vertIndex = i+N*2+1;
		face[i - 1].vert[3].vertIndex = i+N*2;
		
	}
	// Phia sau ben trai
	for (int i = N*2; i < N*3-1;i++){
		face[i - 2].nVerts = 4;
		face[i - 2].vert = new VertexID[face[i - 2].nVerts];
		face[i - 2].vert[0].vertIndex = i-N*2;
		face[i - 2].vert[1].vertIndex = i-N*2+1;
		face[i - 2].vert[2].vertIndex = i+1;
		face[i - 2].vert[3].vertIndex = i;
		
	}
	// Phia truoc ben trai
	for (int i = N*3; i < N*4-1;i++){
		face[i - 3].nVerts = 4;
		face[i - 3].vert = new VertexID[face[i - 3].nVerts];
		face[i - 3].vert[0].vertIndex = i-N*2;
		face[i - 3].vert[1].vertIndex = i-N*2+1;
		face[i - 3].vert[2].vertIndex = i+1;
		face[i - 3].vert[3].vertIndex = i;
		
	}

	//////////// Draw phia cuc ben phai
	// Draw cicle bottom
	for (int i = N*4; i < N*5 - 1; i++){
		face[i - 4].nVerts = 4;
		face[i - 4].vert = new VertexID[face[i - 4].nVerts];
		face[i - 4].vert[0].vertIndex = i;
		face[i - 4].vert[1].vertIndex = i+1;
		face[i - 4].vert[2].vertIndex = i+N+1;
		face[i - 4].vert[3].vertIndex = i+N;
		
	}
	for (int i = N*5; i < N*6-1;i++){
		// Phia tren ben phai
		face[i - 5].nVerts = 4;
		face[i - 5].vert = new VertexID[face[i - 5].nVerts];
		face[i - 5].vert[0].vertIndex = i+N;
		face[i - 5].vert[1].vertIndex = i+N+1;
		face[i - 5].vert[2].vertIndex = i+N*2+1;
		face[i - 5].vert[3].vertIndex = i+N*2;
		
	}
	// Phia sau ben phai
	for (int i = N*6; i < N*7-1;i++){
		face[i - 6].nVerts = 4;
		face[i - 6].vert = new VertexID[face[i - 6].nVerts];
		face[i - 6].vert[0].vertIndex = i-N*2;
		face[i - 6].vert[1].vertIndex = i-N*2+1;
		face[i - 6].vert[2].vertIndex = i+1;
		face[i - 6].vert[3].vertIndex = i;
		
	}
	// Phia truoc ben phai
	for (int i = N*7; i < N*8-1;i++){
		face[i - 7].nVerts = 4;
		face[i - 7].vert = new VertexID[face[i - 7].nVerts];
		face[i - 7].vert[0].vertIndex = i-N*2;
		face[i - 7].vert[1].vertIndex = i-N*2+1;
		face[i - 7].vert[2].vertIndex = i+1;
		face[i - 7].vert[3].vertIndex = i;
		
	}

	int k = N*8-8;
	// Hinh hop o giua
	face[k].nVerts = 4;
	face[k].vert = new VertexID[face[k].nVerts];
	face[k].vert[0].vertIndex = 0;
	face[k].vert[1].vertIndex = N*2;
	face[k].vert[2].vertIndex = N*6;
	face[k].vert[3].vertIndex = N*4;


	face[k+1].nVerts = 4;
	face[k+1].vert = new VertexID[face[k+1].nVerts];
	face[k+1].vert[0].vertIndex = N;
	face[k+1].vert[1].vertIndex = N*3;
	face[k+1].vert[2].vertIndex = N*7;
	face[k+1].vert[3].vertIndex = N*5;


	face[k+2].nVerts = 4;
	face[k+2].vert = new VertexID[face[k+2].nVerts];
	face[k+2].vert[0].vertIndex = 0;
	face[k+2].vert[1].vertIndex = N;
	face[k+2].vert[2].vertIndex = N*5;
	face[k+2].vert[3].vertIndex = N*4;


	face[k+3].nVerts = 4;
	face[k+3].vert = new VertexID[face[k+3].nVerts];
	face[k+3].vert[0].vertIndex = N*2;
	face[k+3].vert[1].vertIndex = N*3;
	face[k+3].vert[2].vertIndex = N*7;
	face[k+3].vert[3].vertIndex = N*6;

	k += 4;
	// Hinh hop o giua
	face[k].nVerts = 4;
	face[k].vert = new VertexID[face[k].nVerts];
	face[k].vert[0].vertIndex = N-1;
	face[k].vert[1].vertIndex = N*2-1;
	face[k].vert[2].vertIndex = N*6-1;
	face[k].vert[3].vertIndex = N*5-1;


	face[k+1].nVerts = 4;
	face[k+1].vert = new VertexID[face[k+1].nVerts];
	face[k+1].vert[0].vertIndex = N*2-1;
	face[k+1].vert[1].vertIndex = N*4-1;
	face[k+1].vert[2].vertIndex = N*8-1;
	face[k+1].vert[3].vertIndex = N*6-1;


	face[k+2].nVerts = 4;
	face[k+2].vert = new VertexID[face[k+2].nVerts];
	face[k+2].vert[0].vertIndex = N*3-1;
	face[k+2].vert[1].vertIndex = N*4-1;
	face[k+2].vert[2].vertIndex = N*8-1;
	face[k+2].vert[3].vertIndex = N*7-1;


	face[k+3].nVerts = 4;
	face[k+3].vert = new VertexID[face[k+3].nVerts];
	face[k+3].vert[0].vertIndex = N-1;
	face[k+3].vert[1].vertIndex = N*3-1;
	face[k+3].vert[2].vertIndex = N*7-1;
	face[k+3].vert[3].vertIndex = N*5-1;

}

void Mesh::CreateBanNguyet(float x, float y, float r, float h){
	const int N = 19;
	float nho = (x - r*2)/2.0;
	float hTam = h - r - nho;
	float a[N], b[N], c[N], d[N], e[N], f[N];
	
	numVerts = N;
	pt = new Point3[numVerts*8+4];
	pt[numVerts*8-1].set(0,hTam, 0);
	int theta = 0;
	// Tim toa do cac dinh (a,b) vanh tren, (c,d) vanh duoi, (e,f) vanh lon.
	float khoangcach = 0;
	for (int i = 0; i < N; i++){
		a[i] = r*cos(theta*3.14/180) + khoangcach;
		b[i] = r*sin(theta*3.14/180);
		theta += 10;
	}
	theta = 180;
	for (int i = 0; i < N; i++){
		c[i] = r*cos(theta*3.14/180) + khoangcach;
		d[i] = r*sin(theta*3.14/180);
		theta += 10;
	}
	theta = 0;
	for (int i = 0; i < N; i++){
		e[i] = (r+nho)*cos(theta*3.14/180) + khoangcach;
		f[i] = (r+nho)*sin(theta*3.14/180);
		theta += 10;
	}

	// Gan vao pt: Nho tren -> nho duoi -> bu tren
	for(int i = 0; i < numVerts;i++){
		pt[i].set(a[i], b[i]+hTam, 0);
	}
	for(int i = numVerts; i < numVerts*2;i++){
		pt[i].set(c[i-numVerts], d[i-numVerts]+hTam, 0);
	}
	for(int i = numVerts*2; i < numVerts*3;i++){
		pt[i].set(e[i-numVerts*2], f[i-numVerts*2]+hTam, 0);
	}
	for(int i = numVerts*3; i < numVerts*4;i++){
		pt[i].set(a[i-numVerts*3], b[i-numVerts*3]+hTam, y);
	}
	for(int i = numVerts*4; i < numVerts*5;i++){
		pt[i].set(c[i-numVerts*4], d[i-numVerts*4]+hTam, y);
	}
	for(int i = numVerts*5; i < numVerts*6;i++){
		pt[i].set(e[i-numVerts*5], f[i-numVerts*5]+hTam, y);
	}
	for(int i = numVerts*6; i < numVerts*7;i++){ 
		pt[i].set(a[i-numVerts*6], 0, 0);
	}
	for(int i = numVerts*7; i < numVerts*8;i++){ 
		pt[i].set(a[i-numVerts*7], 0, y);
	}
	// Set 4 coner
	pt[numVerts*8].set(e[0],0,0);
	pt[numVerts*8+1].set(e[numVerts-1],0,0);
	pt[numVerts*8+2].set(e[0],0,y);
	pt[numVerts*8+3].set(e[numVerts-1],0,y);

	numFaces = N*8;
	face = new Face[numFaces];

	// Draw cicle bottom
	for (int i = 0; i < N-1; i++){
		face[i].nVerts = 4;
		face[i].vert = new VertexID[face[i].nVerts];
		face[i].vert[0].vertIndex = i;
		face[i].vert[1].vertIndex = i+1;
		face[i].vert[2].vertIndex = i+N*2+1;
		face[i].vert[3].vertIndex = i+N*2;
		
	}

	for (int i = 0; i < N-1; i++){
		face[i+N-1].nVerts = 4;
		face[i+N-1].vert = new VertexID[face[i+N-1].nVerts];
		face[i+N-1].vert[0].vertIndex = i+N*3;
		face[i+N-1].vert[1].vertIndex = i+N*3+1;
		face[i+N-1].vert[2].vertIndex = i+N*5+1;
		face[i+N-1].vert[3].vertIndex = i+N*5;
		
	}
	// Draw top top
	for (int i = 0; i < N-1; i++){
		face[i+N*2-2].nVerts = 4;
		face[i+N*2-2].vert = new VertexID[face[i+N*2-2].nVerts];
		face[i+N*2-2].vert[0].vertIndex = i+N*2;
		face[i+N*2-2].vert[1].vertIndex = i+N*2+1;
		face[i+N*2-2].vert[2].vertIndex = i+N*5+1;
		face[i+N*2-2].vert[3].vertIndex = i+N*5;
		
	}
	// Draw top bot
	for (int i = 0; i < N-1; i++){
		face[i+N*3-3].nVerts = 4;
		face[i+N*3-3].vert = new VertexID[face[i+N*3-3].nVerts];
		face[i+N*3-3].vert[0].vertIndex = i;
		face[i+N*3-3].vert[1].vertIndex = i+1;
		face[i+N*3-3].vert[2].vertIndex = i+N*3+1;
		face[i+N*3-3].vert[3].vertIndex = i+N*3;
		
	}
	// Draw mid top
	for (int i = 0; i < N-1; i++){
		face[i+N*4-4].nVerts = 4;
		face[i+N*4-4].vert = new VertexID[face[i+N*4-4].nVerts];
		face[i+N*4-4].vert[0].vertIndex = i+N;
		face[i+N*4-4].vert[1].vertIndex = i+N+1;
		face[i+N*4-4].vert[2].vertIndex = i+N*4+1;
		face[i+N*4-4].vert[3].vertIndex = i+N*4;

	}
	// Draw mid sau
	for (int i = 0; i < N-1; i++){
		face[i+N*5-5].nVerts = 4;
		face[i+N*5-5].vert = new VertexID[face[i+N*5-5].nVerts];
		face[i+N*5-5].vert[0].vertIndex = i+N;
		face[i+N*5-5].vert[1].vertIndex = i+N+1;
		face[i+N*5-5].vert[2].vertIndex = N*7-2-i;
		face[i+N*5-5].vert[3].vertIndex = N*7-1-i;
		
	}
	// Draw mid truoc
	for (int i = 0; i < N-1; i++){
		face[i+N*6-6].nVerts = 4;
		face[i+N*6-6].vert = new VertexID[face[i+N*6-6].nVerts];
		face[i+N*6-6].vert[0].vertIndex = i+N*4;
		face[i+N*6-6].vert[1].vertIndex = i+N*4+1;
		face[i+N*6-6].vert[2].vertIndex = N*8-2-i;
		face[i+N*6-6].vert[3].vertIndex = N*8-1-i;
		
	}
	// Draw 4 coner
	face[N*7-7].nVerts = 4;
	face[N*7-7].vert = new VertexID[face[N*7-7].nVerts];
	face[N*7-7].vert[0].vertIndex = N*6;
	face[N*7-7].vert[1].vertIndex = N*8;
	face[N*7-7].vert[2].vertIndex = N*2;
	face[N*7-7].vert[3].vertIndex = 0;

	///
	face[N*7-6].nVerts = 4;
	face[N*7-6].vert = new VertexID[face[N*7-6].nVerts];
	face[N*7-6].vert[0].vertIndex = N*7;
	face[N*7-6].vert[1].vertIndex = N*8+2;
	face[N*7-6].vert[2].vertIndex = N*5;
	face[N*7-6].vert[3].vertIndex = N*3;

	/////
	face[N*7-5].nVerts = 4;
	face[N*7-5].vert = new VertexID[face[N*7-5].nVerts];
	face[N*7-5].vert[0].vertIndex = N*7-1;
	face[N*7-5].vert[1].vertIndex = N*8+1;
	face[N*7-5].vert[2].vertIndex = N*3-1;
	face[N*7-5].vert[3].vertIndex = N-1;

	///
	face[N*7-4].nVerts = 4;
	face[N*7-4].vert = new VertexID[face[N*7-4].nVerts];
	face[N*7-4].vert[0].vertIndex = N*8-1;
	face[N*7-4].vert[1].vertIndex = N*8+3;
	face[N*7-4].vert[2].vertIndex = N*6-1;
	face[N*7-4].vert[3].vertIndex = N*4-1;

	///// Draw 2 near
	face[N*7-3].nVerts = 4;
	face[N*7-3].vert = new VertexID[face[N*7-3].nVerts];
	face[N*7-3].vert[0].vertIndex = N*8;
	face[N*7-3].vert[1].vertIndex = N*8+2;
	face[N*7-3].vert[2].vertIndex = N*5;
	face[N*7-3].vert[3].vertIndex = N*2;

	///
	face[N*7-2].nVerts = 4;
	face[N*7-2].vert = new VertexID[face[N*7-2].nVerts];
	face[N*7-2].vert[0].vertIndex = N*8+1;
	face[N*7-2].vert[1].vertIndex = N*8+3;
	face[N*7-2].vert[2].vertIndex = N*6-1;
	face[N*7-2].vert[3].vertIndex = N*3-1;

}
