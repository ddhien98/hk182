lst = [1, 40, 30, 0]
n = 4
def lessThan_a(n,lst):
	return [x for x in lst if x < n]

def lessThan_b(n,lst):
	return [lst[0]] + lessThan_b(n, lst[1:]) if lst and lst[0] < n else lessThan_b(n, lst[1:]) if lst else []

def lessThan_c(n,lst):
	return list(filter(lambda x: x<n,lst))

print(lessThan_a(n,lst))


