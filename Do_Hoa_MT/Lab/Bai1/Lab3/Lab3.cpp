// Bai3.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"

#include <math.h>
#include <iostream>
#include "supportClass.h"
#include "Mesh.h"
#include <tchar.h>

using namespace std;

#define PI			3.1415926

int		screenWidth = 600;
int		screenHeight= 600;

bool	bWireFrame = false;

float	base1Radius = 0.8;
float	base1Height = 0.2;
float	base1RotateStep = 5;

float	base2Radius = 0.6;
float	base2Height = 1.2;

float	cylinderRadius = 0.4;
float	cylinderHeight = 2.2;
float	cylinderRotateStep = 5;
float	cylinderTranslationStep = 0.05;
//float	cylinderOffset = base2Height-cylinderHeight/2;
float	cylinderOffset = 0;
float	rotorOffset = 0;

float	ovanStep = 0.75*4/72;
float	ovanOffset = 0;

float	camera_angle;
float	camera_height;
float	camera_dis;
bool	animation = false;
bool	light = true;


/////////////

Mesh	base1;
Mesh	base2;
Mesh	cylinder;
Mesh	ban;
Mesh	giado, giado1, giado2, giado3;
Mesh	rotor;
Mesh	thanhtruot, thanhtruot1;
Mesh	cocaulienket;
Mesh	chot;
Mesh	day;

void myKeyboard(unsigned char key, int x, int y)
{
	float	fRInc;
	float	fAngle;
    switch(key)
    {
	case '1':
		base1.rotateY += base1RotateStep;
		if(base1.rotateY > 360)
			base1.rotateY -=360;
		break;
	case '2':	
		base1.rotateY -= base1RotateStep;
		if(base1.rotateY < 0)
			base1.rotateY +=360;
		break;
	case '3':
		cylinder.rotateY += cylinderRotateStep;
		if(cylinder.rotateY > 360)
			cylinder.rotateY -=360;
		break;
	case '4':	
		cylinder.rotateY -= cylinderRotateStep;
		if(cylinder.rotateY < 0)
			cylinder.rotateY +=360;
		break;
	case '5':
		cylinderOffset += cylinderTranslationStep;
		if(cylinderOffset > base2Height/2)
			cylinderOffset = base2Height/2;
		break;
	case '6':	
		cylinderOffset -= cylinderTranslationStep;
		if(cylinderOffset<0)
			cylinderOffset=0;
		break;
	case '7':
		rotor.rotateY += cylinderRotateStep;
		if(rotor.rotateY > 360)
			rotor.rotateY -=360;

		ovanOffset = 0.75*cos((rotor.rotateY-90)*PI/180);

		break;
	case '8':
		rotor.rotateY -= cylinderRotateStep;
		if(rotor.rotateY < 0)
			rotor.rotateY +=360;

		ovanOffset = 0.75*cos((rotor.rotateY-90)*PI/180);
		break;
	case '+':
		camera_dis += 0.5;
		if (camera_dis < 0){
			camera_dis == 0;
		}
		break;
	case '-':
		camera_dis -= 0.5;
		if (camera_dis < 0){
			camera_dis == 0;
		}
		break;
	case 'a':
		animation = !animation;
		break;
	case 'A':
		animation = !animation;
		break;
	case 'W':
		bWireFrame = !bWireFrame;
		break;
	case 'w':
		bWireFrame = !bWireFrame;
		break;
	case 'd':
		light = !light;
		break;
	case 'D':
		light = !light;
		break;

	}
    glutPostRedisplay();
}

void Animation(){
	if (animation){
		rotor.rotateY += 0.05;
		if(rotor.rotateY > 360)
			rotor.rotateY -=360;

		ovanOffset = 0.75*cos((rotor.rotateY-90)*PI/180);
	}
	glutPostRedisplay();
}

void specialKey(int key, int x, int y){
	switch(key){
	case GLUT_KEY_UP:
		camera_height += 0.5;
		break;
	case GLUT_KEY_DOWN:
		camera_height -= 0.5;
		break;
	case GLUT_KEY_RIGHT:
		camera_angle += 0.5;
		if (camera_angle > 360)
			camera_angle -= 360;
		break;
	case GLUT_KEY_LEFT:
		camera_angle -= 0.5;
		if (camera_angle < 0)
			camera_angle += 360;
		break;

	}
	glutPostRedisplay();
}


void drawAxis()
{
	glColor3f(0, 0, 1);
	glBegin(GL_LINES);
		glColor3f(1, 0, 0);
		glVertex3f(0, 0, 0);//x
		glVertex3f(4, 0, 0);

		glColor3f(0, 1, 0);
		glVertex3f(0, 0, 0);//y
		glVertex3f(0, 4, 0);

		glColor3f(0, 0, 1);
		glVertex3f(0, 0, 0);//z
		glVertex3f(0, 0, 4);
	glEnd();
}

void drawBase1()
{
	glPushMatrix();

		
		GLfloat	myColor[] = {1.0, 0.0, 0.0, 0.0};
		glMaterialfv(GL_FRONT, GL_DIFFUSE, myColor);
		
		glRotatef(base1.rotateY, 0, 1, 0);

		if(bWireFrame)
			base1.DrawWireframe();
		else
			base1.Draw();

	glPopMatrix();
}

void drawBase2()
{
	glPushMatrix();
		GLfloat	myColor[] = {1.0, 0.0, 0.0, 1.0};
		glMaterialfv(GL_FRONT, GL_DIFFUSE, myColor);

		//glTranslated(0, base2Height/2.0+base1Height, 0);
		glTranslated(0, base1Height, 0);
		glRotatef(base1.rotateY, 0, 1, 0);

		if(bWireFrame)
			base2.DrawWireframe();
		else
			base2.Draw();

	glPopMatrix();
}

void drawCylinder()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		glTranslated(0, base1Height+cylinderOffset, 0);
		glRotatef(cylinder.rotateY+base1.rotateY , 0, 1, 0);

		if(bWireFrame)
			cylinder.DrawWireframe();
		else
			cylinder.DrawColor();

	glPopMatrix();
}

void drawBan()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		glTranslated(0, cylinderHeight+0.1+cylinderOffset, 0);
		glRotatef(cylinder.rotateY+base1.rotateY , 0, 1, 0);

		if(bWireFrame)
			ban.DrawWireframe();
		else
			ban.DrawColor();

	glPopMatrix();
}

void drawGiado()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		
		glTranslated(0, cylinderHeight+0.2+cylinderOffset + 0.1, 0);
		glRotatef(cylinder.rotateY+base1.rotateY , 0, 1, 0);
		

		if(bWireFrame)
			giado.DrawWireframe();
		else
			giado.DrawColor();

	glPopMatrix();
}

void drawGiado1()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		glTranslated(0, cylinderHeight+0.2+cylinderOffset + 0.2, 0);
		glRotatef(cylinder.rotateY+base1.rotateY , 0, 1, 0);

		glTranslatef(1.6, 0, 0);
		glRotatef(90, 0, 1, 0);

		if(bWireFrame)
			giado1.DrawWireframe();
		else
			giado1.DrawColor();

	glPopMatrix();
}


void drawGiado2()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		
		glTranslated(0, cylinderHeight+0.2+cylinderOffset + 0.1, 0);
		glRotatef(cylinder.rotateY+base1.rotateY , 0, 1, 0);
		

		if(bWireFrame)
			giado2.DrawWireframe();
		else
			giado2.DrawColor();

	glPopMatrix();
}

void drawGiado3()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		glTranslated(0, cylinderHeight+0.2+cylinderOffset + 0.2, 0);
		glRotatef(cylinder.rotateY+base1.rotateY , 0, 1, 0);

		glTranslatef(-1.6, 0, 0);
		glRotatef(-90, 0, 1, 0);

		if(bWireFrame)
			giado3.DrawWireframe();
		else
			giado3.DrawColor();

	glPopMatrix();
}

void drawRotor()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		glTranslated(0, cylinderHeight+0.2+cylinderOffset + 0.2, 0);
		glRotatef(cylinder.rotateY+base1.rotateY+rotor.rotateY , 0, 1, 0);


		if(bWireFrame)
			rotor.DrawWireframe();
		else
			rotor.DrawColor();

	glPopMatrix();
}

void drawThanhtruot()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		glTranslated(0, cylinderHeight+0.2+cylinderOffset + 0.2  + 0.4, 0);
		glRotatef(cylinder.rotateY+base1.rotateY , 0, 1, 0);

		glTranslatef(ovanOffset, 0, 0);

		glTranslatef(-0.3, 0, 0);
		glRotatef(90, 0, 0, 1);


		if(bWireFrame)
			thanhtruot.DrawWireframe();
		else
			thanhtruot.DrawColor();

	glPopMatrix();
}

void drawThanhtruot1()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		glTranslated(0, cylinderHeight+0.2+cylinderOffset + 0.2  + 0.4, 0);
		glRotatef(cylinder.rotateY+base1.rotateY , 0, 1, 0);

		glTranslatef(ovanOffset, 0, 0);

		glTranslatef(0.3, 0, 0);
		glRotatef(-90, 0, 0, 1);


		if(bWireFrame)
			thanhtruot1.DrawWireframe();
		else
			thanhtruot1.DrawColor();

	glPopMatrix();
}

void drawCocaulienket()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		glTranslated(0, cylinderHeight+0.2+cylinderOffset + 0.2 + 0.2, 0);
		glRotatef(cylinder.rotateY+base1.rotateY , 0, 1, 0);

		glTranslatef(ovanOffset, 0, 0);
		glRotatef(90, 0, 1, 0);

		if(bWireFrame)
			cocaulienket.DrawWireframe();
		else
			cocaulienket.DrawColor();

	glPopMatrix();
}

void drawChot()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		glTranslated(0, cylinderHeight+0.2+cylinderOffset + 0.2 + 0.2, 0);
		glRotatef(cylinder.rotateY+base1.rotateY+rotor.rotateY , 0, 1, 0);

		glTranslatef(0, 0, 0.75);


		if(bWireFrame)
			chot.DrawWireframe();
		else
			chot.DrawColor();

	glPopMatrix();
}

void drawHang(float x){
	for(int i = 0; i <= 5; i++){
		glPushMatrix();
		if(x == 0){
			glTranslated(i*(2*sin(PI/3)), 0, 0);
		}
		else{
			glTranslated(i*(2*sin(PI/3)), 0, 0 + x*3);
		}
		day.DrawColor();

		glPopMatrix();
	
	}
	for(int i = 1; i <= 4; i++){
		glPushMatrix();
		
		if(x == 0){
			glTranslated(-i*(2*sin(PI/3)), 0, 0);
		}
		else{
			glTranslated(-i*(2*sin(PI/3)), 0, 0 + x*3);
		}

		day.DrawColor();

		glPopMatrix();
	
	}
}
void drawHangVer2(float x){
	glPushMatrix();
	glTranslated(sin(PI/3), 0, 1+cos(PI/3) + x*3);
	drawHang(0);
	glPopMatrix();
}
void drawDay()
{
	for (int i = -7; i < 7; i++){
		drawHang(i);
		drawHangVer2(i);
	}
	
}

void setupMaterial(float ambient[], float diffuse[], float specular[], float shiness)
{
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shiness);
}


void myDisplay()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(camera_angle, camera_height, camera_dis, 0, 1.0, 0, 0, 1, 0);

	GLfloat	lightDiffuse[]		={0.5f, 0.5f, 0.5f, 0.5f};
	GLfloat	lightSpecular[]		={0.5f, 0.5f, 0.5f, 0.2f};
	GLfloat	lightAmbient[]		={0.5f, 0.5f, 0.5f, 0.5f};
	GLfloat light_position1[]	={7.0f, 10.0f, -5.0f, 1.0f};
	

	glLightfv(GL_LIGHT0, GL_POSITION, light_position1);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);

	GLfloat	lightDiffuse2[]={0.7,0.7,0.7,1.0f};
	GLfloat	lightSpecular2[]={1,1,1,0.1};
	GLfloat	lightAmbient2[]={1,1,1,0.1};
	GLfloat light_position2[]={0, 7.0, 5, 0.0f};

	glLightfv(GL_LIGHT1, GL_POSITION, light_position2);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lightDiffuse2);
	glLightfv(GL_LIGHT1, GL_AMBIENT, lightAmbient2);
	glLightfv(GL_LIGHT1, GL_SPECULAR, lightSpecular2);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0); 
	glEnable(GL_LIGHT1);



	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	
	glViewport(0, 0, screenWidth, screenHeight);

	
	//drawAxis();

	drawBase1();
	drawBase2();
	drawCylinder();
	drawBan();
	drawGiado();
	drawGiado1();
	drawGiado2();
	drawGiado3();
	drawRotor();
	drawThanhtruot();
	drawThanhtruot1();
	drawCocaulienket();
	drawChot();
	drawDay();
	
	glFlush();
    glutSwapBuffers();
}

void myInit()
{
	float	fHalfSize = 4;
	camera_angle = 3;
	camera_dis = 8;
	camera_height = 4;


	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	glFrontFace(GL_CCW);
	glEnable(GL_DEPTH_TEST);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-fHalfSize, fHalfSize, -fHalfSize, fHalfSize, -1000, 1000);
}

int _tmain(int argc, _TCHAR* argv[])
{
	glutInit(&argc, (char**)argv); //initialize the tool kit
	glutInitDisplayMode(GLUT_DOUBLE |GLUT_RGB | GLUT_DEPTH);//set the display mode
	glutInitWindowSize(screenWidth, screenHeight); //set window size
	glutInitWindowPosition(100, 100); // set window position on screen
	glutCreateWindow("Lab 3 - Demo (2018-2019)"); // open the screen window

	base1.CreateTR(base2Radius, base2Height);
	//base1.CreateDeGia(1.8);
	base1.CalculateFacesNorm();
	//base1.SetColor(0);

	//base2.CreateTR(base2Radius, base2Height);
	//base2.CalculateFacesNorm();
	//base2.SetColor(0);

	//cylinder.CreateTR(cylinderRadius, cylinderHeight+0.2);
	//cylinder.SetColor(2);

	//ban.CreateCuboid(3, 0.1, 1.5);
	//ban.SetColor(1);

	//giado.CreateDeGia(1.8); // x = 0.5, z = 0.2
	//giado.SetColor(7);

	//giado1.CreateBanNguyet(0.5, 0.4, 0.15, 0.7);
	//giado1.SetColor(7);

	//giado2.CreateDeGia(-1.8);
	//giado2.SetColor(7);

	//giado3.CreateBanNguyet(0.5, 0.4, 0.15, 0.7);
	//giado3.SetColor(7);

	//rotor.CreateTR(1.2, 0.2); // Du ra 0.1
	//rotor.SetColor(14);

	//thanhtruot.CreateTruVer2(0.2, 0.3, 0.15, 2.2, 0.1);
	//thanhtruot.SetColor(3);

	//thanhtruot1.CreateTruVer2(0.2, 0.3, 0.15, 2.2, 0.1);
	//thanhtruot1.SetColor(3);

	//cocaulienket.CreateOvan(0.3, 1.5, 0.14, 0.37); // y = 1.5
	//cocaulienket.SetColor(3);

	//chot.CreateTruVer2(0.14, 0.35, 0.1, 0, 0.1);
	//chot.SetColor(14);

	//day.CreateHinhThoi(1);

	

	myInit();

	
	glutKeyboardFunc(myKeyboard);
	glutSpecialFunc(specialKey);
    glutDisplayFunc(myDisplay);
	glutIdleFunc(Animation);
	  
	glutMainLoop();
	return 0;
}

