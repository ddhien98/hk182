#include <windows.h>
#include <glut.h>
#include <math.h>
#include <iostream>

using namespace std;

int screenWidth = 500;
int screenHeight = 500;
int len = 20;

void drawEllipse(float cx, float cy, float rx, float ry, float angle) 
{ 
	int num_segments = 36;
    float theta = 2 * 3.1415926 / float(num_segments); 
    float c = cosf(theta);//precalculate the sine and cosine
    float s = sinf(theta);
    float t;

    float x = cos(30*3.14/180);//we start at angle = 0 
    float y = sin(30*3.14/180); 

	/*
	glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
	glTranslatef(cx, cy, 1);
	glRotatef(angle, 0, 0, 1);*/

    glBegin(GL_LINE_LOOP); 
    for(int ii = 0; ii < num_segments; ii++) 
    { 
		int goc = ii * 10 + 30;
        //apply radius and offset
        glVertex2f(x * rx + cx, y * ry + cy);//output vertex 

        //apply the rotation matrix
        t = x;
        x = (c * x - s * y);
        y = (s * t + c * y);
    } 
    glEnd(); 
}

void drawCau4C(){
	for(int i = 0; i < 6;i++){
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		//glRotatef(i*30, 1, 0, 1);
		drawEllipse(1, 0, 1, 0.5, i*30);
	}

}

void drawCilcle(){
	float angle = 18;
	float r = 1;
	float x, y;
	glBegin(GL_LINE_LOOP);
	for(int i = 0; i < 360; i += angle){
		x = r*cos(i*3.14/180);
		y = r*sin(i*3.14/180);
		glVertex2f(x,y);
	}
	glEnd();
}


void drawSquare(int x, int y) {
	//y = screenHeight-y; /* invert y position */
	glColor3ub( (char) rand()%256, (char) rand ()%256, (char) rand()%256);
	glBegin(GL_POLYGON);
		glVertex2f(x+len, y+len);  glVertex2f(x-len, y+len);
		glVertex2f(x-len, y-len);  glVertex2f(x+len, y-len);
	glEnd();
	glFlush();
}

void init()
{
    

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
 //   //glOrtho(0, screenWidth, 0, screenHeight, -1, 1);
 //   glOrtho(0, screenWidth, screenHeight, 0, -1, 1);
	//glClearColor(1, 1, 1, 1);
 //   glColor3f(1, 0, 0);
	gluOrtho2D(-2.0, 2.0, -2.0, 2.0);	
    glMatrixMode(GL_MODELVIEW);
    glClearColor(1.0, 1.0, 1.0,1.0);
    glColor3f(0.0,0.0,0.0);

}

void drawLine(float x1, float y1, float len, float angle, float &x2, float &y2){
	x2 = x1 + cos(angle*3.14/180)*len;
	y2 = y1 + sin(angle*3.14/180)*len;
	glBegin(GL_LINES);
		glVertex2f(x1, y1);
		glVertex2f(x2, y2);
	glEnd();
}

void drawTriangle(float x1, float y1, float len, float angle){
	float bx, by, cx, cy;
	drawLine(x1, y1, len, angle, bx, by);
	drawLine(x1, y1, len, angle + 60, cx, cy);
	drawLine(bx, by, len, angle + 120, cx, cy);
}

void drawCau4(){
	for(int i = 0; i < 360;i += 36){
		drawTriangle(0, 0, 1, i);
	}
}

void drawTuGiac(float x, float y, float len, float angle){
	float a, b;
	drawLine(x, y, len, angle, a, b);
	drawLine(x, y, len, angle + 90, a, b);
	drawLine(a, b, len, angle, a, b);
	drawLine(a, b, len, angle - 90, a, b);
}

void drawCau5a(){
	for(int i = 0; i < 360; i += 30){
		drawTuGiac(0,0,0.5,i);
	}
}

void draw10Canh(float x, float y, float len, float angle){
	float a, b;
	a = x; b = y;
	for(int i = 0; i < 360; i+= 36){
		drawLine(a, b, len, angle + i, a, b);
	}
}

void drawCau5b(){
	for(int i = 0; i<360;i+= 30){
		draw10Canh(0, 0, 0.5, i);
	}
}

void mydisplay(){
	glClear(GL_COLOR_BUFFER_BIT);
	//drawEllipse(1, 0, 1, 1.5, 90);
	drawCau4C();
	glFlush();
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(0, 0);
    glutCreateWindow("Bai tap");

    glutDisplayFunc(mydisplay);
    init();

    glutMainLoop();
}

