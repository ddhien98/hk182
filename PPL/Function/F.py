# def compose (*functions):
#     def inner(arg):
#         for f in reversed(functions):
#             arg = f(arg)
#         return arg
#     return inner
# def square (x):
#         return x ** 2
# def increment (x):
#         return x + 1
# def half (x):
#         return x / 2

# composed = compose(square, increment, half) # square(increment(half(x)))
# print(composed(5))

############## Cau 1
from functools import reduce
# def compose(*g):
# 	def h(args):
# 		return reduce(lambda x,y: y(x),reversed(g),args)
# 	return h
# def square(x):
# 	return x * x
# def increase(x):
# 	return x + 1
# def double(x):
# 	return x * 2

# m = compose(square, increase, double)
# print(m(5))

# def compose(f, g):
# 	def h(x):
# 		return f(g(x))
# 	return h
# def square(x):
# 	return x * x
# def increase(x):
# 	return x + 1
# def double(x):
# 	return x * 2

# m = compose(double, increase)
# print(m(5))
# m = compose(square, compose(increase, double))
# print(m(5))

######## Cau 2
#def foo(x:int)(y:float)

############## Cau 3
def f(y):
	def g(x):
		return x-y
	return g

a = f(3)
print(a(2))

####### Cau 5
# L.exists(_%2 == 1)
#List(1, 3, 8).foldLeft(100)(_ - _) == ((100 - 1) - 3) - 8 == 88
#List(1, 3, 8).foldRight(100)(_ - _) == 1 - (3 - (8 - 100)) == -94
# List().foldLeft(list())((x,y)=> y::x)

# lst = [1,2,3,4]
# # print(reduce(lambda x,y:str(x)+str(y),lst)) #Thua str(x) => x
# lst = [1,2,3,4]
# print(reduce(lambda x,y:x+str(y),lst,""))

################## DIST[3,4,5] => [(2,3), (2,4), (2,5)]
# def dist(x,lst):
# 	return list(map(lambda y:(x,y),lst))

# lst = [2,3,4]
# print(dist(1,lst))

# def dist(a,n1):
# 	def dist_h(m1,m2): ## m1 la cai mang truyen zo, m2 la ket qua
# 		if m1:
# 			dist_h(m1[:-1],m2) ## Bo di thang cuoi
# 			m2.append((a,m1[-1])) ## chen thang cuoi zo
# 	ret = []
# 	dist_h(n1,ret)
# 	return ret

# lst = [2,3,4]
# print(dist(1,lst))

# def foo(*args):
# 	if len(args) == 2:
# 		print(args[0],args[1])

# lst = [2,3,4]
# print(reduce(lambda x,y:x+y,lst,0)) # 0 la gia tri khoi dong

