
import imp
import time
# ======================================================================


class Piece:
    def __init__(self, animal, pos):
        self.type = animal
        self.position = pos

    def __str__(self):
        return str(self.type) + ': ' + str(self.position)


class Board:
    def __init__(self, l_black=[], l_red=[]):
        self.list_black = l_black
        self.list_red = l_red

    def print(self):
        print('blacks: ' + str(len(self.list_black)))
        for item in self.list_black:
            print(item)
        print('\nreds: ' + str(len(self.list_red)))
        for item in self.list_red:
            print(item)
        print()


# ======================================================================

# Student SHOULD implement this function to change current state to new state properly
def doit(move, board):
    name = None

    for x in board.list_black:
        if move[0] == x:
            name = "black"
            break

    if name is "black":
        for x in board.list_black:
            if move[0] == x:
                new = (move[1][0], move[1][1])
                x.position = new

            if move[1] == (1, 4):
                print("Black WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW")
                input()
                break

        for x in board.list_red:
            if move[1] == x.position:
                board.list_red.remove(x)
    else:
        for x in board.list_red:
            if move[0] == x:
                new = (move[1][0], move[1][1])
                x.position = new

            if move[1] == (9, 4):
                print("Red WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW")
                input()
                break

        for x in board.list_black:
            if move[1] == x.position:
                board.list_black.remove(x)

    return board
    


# ======================================================================
list_black = [Piece('Voi', (7, 7)), Piece('SuTu', (9, 1)), Piece('Ho', (9, 7)),
               Piece('Bao', (7, 3)), Piece('Soi', (7, 5)), Piece('Cho', (8, 2)),
               Piece('Meo', (8, 6)), Piece('Chuot', (7, 1))]
list_red = [Piece('Voi', (3, 1)), Piece('SuTu', (1, 7)), Piece('Ho', (1, 1)),
              Piece('Bao', (3, 5)), Piece('Soi', (3, 3)), Piece('Cho', (2, 6)),
              Piece('Meo', (2, 2)), Piece('Chuot', (3, 7))]

# list_black = [Piece("Chuot", (7, 3))]
# list_red = [Piece("Voi", (3, 7))]

Initial_Board = Board(list_black, list_red)
# ======================================================================


def play(student_a, student_b, start_state=Initial_Board):
    player_a = imp.load_source(student_a, student_a + ".py")
    player_b = imp.load_source(student_b, student_b + ".py")

    a = player_a.Player('black')
    b = player_b.Player('red')

    curr_player = a
    state = start_state

    board_num = 0

    state.print()

    while True:
        print("It is ", curr_player, "'s turn")

        start = time.time()
        move = curr_player.next_move(state) # tra ve mang piece, new pos
        elapse = time.time() - start

        # print(move)

        if not move:
            break

        print("The move is : [", move[0].type, ': ', move[0].position, '-> ', move[1], end="] ")
        print(" (in %.2f ms)" % (elapse*1000), end=" ")
        if elapse > 100.0:
            print(" ** took more than three second!!", end=" ")
            break
        print()
        # check_move
        state = doit(move, state)
        board_num += 1
        print('board num = ', board_num)

        state.print()

        if curr_player == a:
            curr_player = b
        else:
            curr_player = a

    print("Game Over")
    if curr_player == a:
        print("The Winner is:", student_b, 'red')
    else:
        print("The Winner is:", student_a, 'black')


play("1611089", "1")
