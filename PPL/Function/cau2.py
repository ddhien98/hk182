lst = [[1, 2, 'a'], ['b', 'c']]

# def flatten_a(lst):
# 	temp = []
# 	for x in lst:
# 		if isinstance(x, list):
# 			print("e")
# 			temp = temp + x
# 		else:
# 			temp = temp + [x]
			
# 	return temp
from functools import reduce

# def flatten_b(lst):
# 	return lst[0] + flatten_b(lst[1:]) if lst else []

def flatten_c(lst):
	return reduce(lambda x, y: x+y, lst)

print(flatten_c(lst))