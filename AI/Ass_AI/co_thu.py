from main import Piece, Board
from copy import deepcopy
import random
# ======================== Class Player =======================================

#Một số luật cần tuân thủ:
# 1. Quân mình không được đi vào ô hang của mình
# 2. Khi vào bẫy của mình thì không mất sức mạnh (tránh bẫy của mình)
# 3. Thú bên ngoài bẫy có thể ăn quân trong bẫy (sau đó mới mắc bẫy đối phương)
# 4. Không được tự sát (nhảy vào quân địch mạnh hơn)
# 5. Hổ, Báo, Sư Tử nhảy qua sông mà không quan tâm có quân dưới sông hay không
# 6. Hổ, Báo, Sư Tử không bơi (chỉ nhảy)
# 7. Voi không thể giẫm chuột (trừ trường hợp chuột dính bẫy)

def move_all_state(state, name):
    lst_move = []
    if name is "black":
        for i in state.list_black:
            lst_move += move_rule(i, state, name)
    else:
        for i in state.list_red:
            lst_move += move_rule(i, state, name)

    return lst_move

def move_rule(piece, state, name):
    if piece.type is "SuTu" or piece.type is "Ho":
        return move_Sutu_Ho(piece, state, name)
    elif piece.type is "Bao":
        return move_Bao(piece, state, name)
    elif piece.type is "Meo":
        return move_Meo(piece, state, name)
    else: #piece.type in ["Voi", "Soi", "Cho", "Chuot"]:
        return move_Else(piece, state, name)



def move_Else(piece, state, name):
    lst_move = []

    down = (piece.position[0] - 1, piece.position[1])
    up = (piece.position[0] + 1, piece.position[1])
    right = (piece.position[0], piece.position[1] + 1)
    left = (piece.position[0], piece.position[1] - 1)
    ## check hien tai co nam trong lake ko
    if check_pos_in_lake(piece.position) != 0:
        for i in [left, right, up, down]:
            check = True
            for x in state.list_black:
                if i == x.position:
                    check = False
                    break
            for x in state.list_red:
                if i == x.position:
                    check = False
                    break
            if check:
                lst_move.append(Piece(piece.type, i))
    else:
        for i in [left, right, up, down]:
            if check_pos_in_lake(i) != 0: ## Neu nam trong lake thi check xem tai do co ai chua
                check = True
                for x in state.list_black:
                    if i == x.position:
                        check = False
                        break
                for x in state.list_red:
                    if i == x.position:
                        check = False
                        break

                if check:
                    lst_move.append(Piece(piece.type, i))
            else:
                if check_pos_in_ground(Piece(piece.type, i), state, name) is False:
                    continue
                else:
                    lst_move.append(Piece(piece.type, i))

    return lst_move

def move_Meo(piece, state, name):
    lst_move = []

    down = (piece.position[0] - 1, piece.position[1])
    up = (piece.position[0] + 1, piece.position[1])
    right = (piece.position[0], piece.position[1] + 1)
    left = (piece.position[0], piece.position[1] - 1)
    for i in [left, right, up, down]:
        if check_pos_in_lake(i) != 0: ## Neu nam trong lake
            continue
        else:
            if check_pos_in_ground(Piece(piece.type, i), state, name) is False:
                continue
            else:
                lst_move.append(Piece(piece.type, i))

    return lst_move

def move_Bao(piece, state, name):
    lst_move = []

    down = (piece.position[0] - 1, piece.position[1])
    up = (piece.position[0] + 1, piece.position[1])
    right = (piece.position[0], piece.position[1] + 1)
    left = (piece.position[0], piece.position[1] - 1)
    for i in [left, right, up, down]:
        if check_pos_in_lake(i) != 0: ## Neu nam trong lake
            if i is left:
                if check_pos_in_ground(Piece(piece.type, (piece.position[0], piece.position[1] - 3)), state, name):
                    lst_move.append(Piece(piece.type, (piece.position[0], piece.position[1] - 3)))
            elif i is right:
                if check_pos_in_ground(Piece(piece.type, (piece.position[0], piece.position[1] + 3)), state, name):
                    lst_move.append(Piece(piece.type, (piece.position[0], piece.position[1] + 3)))
            elif i is up or i is down:
                continue
        else:
            if check_pos_in_ground(Piece(piece.type, i), state, name) is False:
                continue
            else:
                lst_move.append(Piece(piece.type, i))

    return lst_move


## return lst move (Piece("", newpos))
def move_Sutu_Ho(piece, state, name):
    lst_move = []

    down = (piece.position[0] - 1, piece.position[1])
    up = (piece.position[0] + 1, piece.position[1])
    right = (piece.position[0], piece.position[1] + 1)
    left = (piece.position[0], piece.position[1] - 1)
    for i in [left, right, up, down]:
        if check_pos_in_lake(i) != 0: ## Neu nam trong lake
            
            if i is left:
                if check_pos_in_ground(Piece(piece.type, (piece.position[0], piece.position[1] - 3)), state, name):
                    lst_move.append(Piece(piece.type, (piece.position[0], piece.position[1] - 3)))
            elif i is right:
                if check_pos_in_ground(Piece(piece.type, (piece.position[0], piece.position[1] + 3)), state, name):
                    lst_move.append(Piece(piece.type, (piece.position[0], piece.position[1] + 3)))
            elif i is up:
                if check_pos_in_ground(Piece(piece.type, (piece.position[0] + 4, piece.position[1])), state, name):
                    lst_move.append(Piece(piece.type, (piece.position[0] + 4, piece.position[1])))
            elif i is down:
                if check_pos_in_ground(Piece(piece.type, (piece.position[0] - 4, piece.position[1])), state, name):
                    lst_move.append(Piece(piece.type, (piece.position[0] - 4, piece.position[1])))
        else:
            if check_pos_in_ground(Piece(piece.type, i), state, name) is False:
                continue
            else:
                lst_move.append(Piece(piece.type, i))


    return lst_move


## return true if move success else return false
def check_pos_in_ground(piece, state, name):
    ## out range
    if piece.position[0] < 1 or piece.position[0] > 9 or piece.position[1] < 1 or piece.position[1] > 7:
        return False

    ## not move hang
    if name is "black" and piece.position == (9, 4):
        return False
    if name is "red" and piece.position == (1, 4):
        return False

    if name is "black":
        for x in state.list_black:
            if x.position == piece.position:
                return False

        for x in state.list_red:
            if x.position == piece.position:
                return check_strong(piece, x, name)

        return True
    else:
        for x in state.list_red:
            if x.position == piece.position:
                return False

        for x in state.list_black:
            if x.position == piece.position:
                return check_strong(piece, x, name)

        return True


## return true if a strong b else return false (a, b is type)
def check_strong(p1, p2, name):
    strong_a = 0
    strong_b = 0
    a = p1.type
    b = p2.type
    if name is "black": ## Den di thi check p2 co trong bay ko
        if p2.position in [(9, 3), (9, 5), (8, 4)]:
            return True
    else:
        if p2.position in [(1, 3), (1, 5), (2, 4)]:
            return True

    if a is "Chuot" and b is "Voi":
        return True

    if a is "Voi" and b is "Chuot":
        return False

    if a is "Voi":
        strong_a = 8
    elif a is "SuTu":
        strong_a = 7
    elif a is "Ho":
        strong_a = 6    
    elif a is "Bao":
        strong_a = 5
    elif a is "Soi":
        strong_a = 4
    elif a is "Cho":
        strong_a = 3
    elif a is "Meo":
        strong_a = 2
    elif a is "Chuot":
        strong_a = 1

    if b is "Voi":
        strong_b = 8
    elif b is "SuTu":
        strong_b = 7
    elif b is "Ho":
        strong_b = 6    
    elif b is "Bao":
        strong_b = 5
    elif b is "Soi":
        strong_b = 4
    elif b is "Cho":
        strong_b = 3
    elif b is "Meo":
        strong_b = 2
    elif b is "Chuot":
        strong_b = 1

    if strong_a >= strong_b:
        return True
    else:
        return False


## return 1 if in lake left and 1 lake right else return 0
def check_pos_in_lake(pos):
    lst_lake_left = [(4, 2), (4, 3), (5, 2), (5, 3), (6, 2), (6, 3)]
    lst_lake_right = [(4, 5), (4, 6), (5, 5), (5, 6), (6, 5), (6, 6)]
    for x in lst_lake_left:
        if x == pos:
            return 1

    for x in lst_lake_right:
        if x == pos:
            return 1

    return 0

## -------------------------------- Xong Rule -------------------------------- ##

# def tinh_min_trong_lst_move(state, name, name2):
#     minValue = 99999

#     lst_move = move_all_state(state, name2)

#     for move in lst_move:
#         new_board = next_board(move, state, name2)
#         boardValue = evaluateBoard(new_board, name)
#         #print(move)
#         if boardValue < minValue:
#             minValue = boardValue

#     return minValue


## return [bestMove, value]
def calculateBestMove(state, name, depth, isMax):
    bestMove = []
    bestValue = -99999

    lst_move = move_all_state(state, name)

    for move in lst_move:
        new_board = next_board(move, state, name)

        boardValue = minimax(new_board, name, "black" if name is "red" else "red", depth, -99999, 99999, not isMax)

        #boardValue = tinh_min_trong_lst_move(new_board, name, "black" if name is "red" else "red")
        
        if boardValue > bestValue:
            bestValue = boardValue
            bestMove = move
            
    return bestMove, bestValue


###  return bestValue
def minimax(state, name, name2, depth, alpha, beta, isMax):
    if depth == 0:
        return evaluateBoard(state, name)

    lst_move = move_all_state(state, name2)
    if isMax:
        bestValue = -99999
        for move in lst_move:
            new_board = next_board(move, state, name2)
            bestValue = max(bestValue ,minimax(new_board, name, "black" if name2 is "red" else "red", depth - 1, alpha, beta, not isMax))

            alpha = max(alpha, bestValue)
            if beta <= alpha:
                return bestValue

        return bestValue
    else:
        bestValue = 99999
        for move in lst_move:
            new_board = next_board(move, state, name2)
            bestValue = min(bestValue ,minimax(new_board, name, "black" if name2 is "red" else "red", depth - 1, alpha, beta, not isMax))

            beta = min(beta, bestValue)
            if beta <= alpha:
                return bestValue

        return bestValue




## return new board
def next_board(move, state, name):
    list_black = deepcopy(state.list_black)
    list_red = deepcopy(state.list_red)

    if name is 'black':
        for piece in list_black:
            if move.type == piece.type:
                new_posistion = move.position
                piece.position = new_posistion

        for piece in list_red:
            if move.position == piece.position:
                list_red.remove(piece)
    elif name is 'red':  
        for piece in list_red:
            if move.type == piece.type:
                new_posistion = move.position
                piece.position = new_posistion

        for piece in list_black:
            if move.position == piece.position:
                list_black.remove(piece)

    new_board = Board(list_black, list_red)
    return new_board


## return value board of player OKKKK
def evaluateBoard(state, name):
    value = 0
    for x in state.list_black:
        value += (getPieceValue(x, "black") if name is "black" else -getPieceValue(x, "black"))
    
    for x in state.list_red:
        value += (getPieceValue(x, "red") if name is "red" else -getPieceValue(x, "red"))

    return value


def getPieceValue(piece, name): ## On roi
    Voi = [
        [0, 1, 0.5, 0, 0.5, 1, 0],
        [1, 5, 8, 3, 8, 5, 1],
        [8, 9, 11, 15, 8, 5, 0],
        [2, 0, 0, 20, 0, 0, 2],
        [4, 0, 0, 25, 0, 0, 2.2],
        [6, 0, 0, 30, 0, 0, 2.4],
        [8, 35, 45, 35, 45, 35, 10],
        [30, 45, 50, 100, 50, 45, 10],
        [15, 25, 100, 9999, 100, 25, 0]
    ]

    SuTu = [
        [0, 8.5, 5.5, 0, 5.5, 8.5, 0],
        [5.5, 12.5, 10.5, 5.5, 10.5, 12.5, 5.5],
        [10.5, 20.5, 21.5, 25.5, 21.5, 20.5, 10.5],
        [12.5, 0, 0, 35.5, 0, 0, 12.5],
        [15.5, 0, 0, 38.5, 0, 0, 15.5],
        [18.5, 0, 0, 40.5, 0, 0, 18.5],
        [20.5, 40.5, 50.5, 45.5, 50.5, 40.5, 20.5],
        [25.5, 50.5, 55.5, 100, 55.5, 50.5, 25.5],
        [0, 30.5, 100, 9999, 100, 30.5, 0]
    ]

    Ho = [
        [0, 8.5, 5.5, 0, 5.5, 8.5, 0],
        [5.5, 12.5, 10.5, 5.5, 10.5, 12.5, 5.5],
        [10.5, 20.5, 21.5, 25.5, 21.5, 20.5, 10.5],
        [12, 0, 0, 35, 0, 0, 12],
        [15, 0, 0, 38, 0, 0, 15],
        [18, 0, 0, 40, 0, 0, 18],
        [20, 40, 50, 45, 50, 40, 20],
        [25, 50, 55, 100, 55, 50, 25],
        [0, 30, 100, 9999, 100, 30, 0]
    ]

    Bao = [
        [0, 2, 1, 0, 1, 2, 0],
        [2, 10, 13, 5, 13, 10, 2],
        [8, 13, 15, 17, 15, 13, 8],
        [8.2, 0, 0, 20, 0, 0, 8.2],
        [8.4, 0, 0, 25, 0, 0, 8.4],
        [8.6, 0, 0, 30, 0, 0, 8.6],
        [10, 30, 45, 35, 45, 35, 10],
        [8, 45, 50, 100, 50, 45, 8],
        [0, 25, 100, 9999, 100, 25, 0]
    ]

    Soi = [
        [0, 2, 1, 0, 1, 2, 0],
        [2, 5, 7, 3, 7, 5, 2],
        [5, 7, 8, 9, 7.2, 7, 5],
        [5.2, 7.3, 12, 15, 7.4, 7.3, 5.2],
        [5.4, 7.4, 15, 20, 7.5, 7.4, 5.4],
        [5.6, 7.5, 20, 30, 7.6, 7.5, 5.6],
        [8, 25, 40, 35, 40, 25, 8],
        [6, 35, 45, 100, 45, 35, 6],
        [0, 20, 100, 9999, 100, 25, 0]
    ]

    Cho = [
        [0, 0.5, 0.3, 0, 0.3, 0.5, 0],
        [0.5, 0.7, 1, 0.5, 1, 0.7, 1],
        [1, 1.1, 1.2, 1.3, 1.2, 1.1, 1],
        [1.2, 1.25, 1.3, 1.4, 1.3, 1.25, 1.2],
        [1.3, 1.35, 1.4, 1.5, 1.4, 1.35, 1.3],
        [1.4, 1.45, 1.5, 1.6, 1.5, 1.45, 1.4],
        [1.5, 3, 4, 4.5, 4, 3, 1.5],
        [2, 3, 4.5, 50, 4.5, 3, 2],
        [0, 2.5, 50, 9999, 50, 2.5, 0]
    ]

    Meo = [
        [0, 0.5, 0.3, 0, 0.3, 0.5, 0],
        [0.5, 0.7, 1, 0.5, 1, 0.7, 1],
        [1, 1.1, 1.2, 1.3, 1.2, 1.1, 1],
        [1.2, 0, 0, 1.4, 0, 0, 1.2],
        [1.3, 0, 0, 1.5, 0, 0, 1.3],
        [1.4, 0, 0, 1.6, 0, 0, 1.4],
        [1.5, 3, 4, 4.5, 4, 3, 1.5],
        [2, 3, 4.5, 30, 4.5, 3, 2],
        [0, 2.5, 30, 9999, 30, 2.5, 0]
    ]

    Chuot = [
        [0, 0.5, 0.2, 0, 0.2, 0.5, 0],
        [0.5, 0.7, 1, 0.2, 1, 0.7, 1],
        [1, 1.1, 1.2, 1.3, 1.2, 1.1, 1],
        [1.2, 1.25, 1.3, 1.4, 1.3, 1.25, 1.2],
        [1.3, 1.35, 1.4, 1.5, 1.4, 1.35, 1.3],
        [1.4, 1.45, 1.5, 1.6, 1.5, 1.45, 1.4],
        [1.5, 3, 4, 4.5, 4, 3, 1.5],
        [2, 3, 4.5, 50, 4.5, 3, 2],
        [0, 2.5, 50, 9999, 50, 2.5, 0]
    ]

    if piece.type is "Chuot":
        return 100 + (Chuot[10 - piece.position[0] - 1][piece.position[1] - 1] if name is "black" else Chuot[piece.position[0] - 1][8 - piece.position[1] - 1])
    elif piece.type is "Meo":
        return 150 + (Meo[10 - piece.position[0] - 1][piece.position[1] - 1] if name is "black" else Meo[piece.position[0] - 1][8 - piece.position[1] - 1])
    elif piece.type is "Cho":
        return 200 + (Cho[10 - piece.position[0] - 1][piece.position[1] - 1] if name is "black" else Cho[piece.position[0] - 1][8 - piece.position[1] - 1])
    elif piece.type is "Bao":
        return 400 + (Bao[10 - piece.position[0] - 1][piece.position[1] - 1] if name is "black" else Bao[piece.position[0] - 1][8 - piece.position[1] - 1])
    elif piece.type is "Ho":
        return 600 + (Ho[10 - piece.position[0] - 1][piece.position[1] - 1] if name is "black" else Ho[piece.position[0] - 1][8 - piece.position[1] - 1])
    elif piece.type is "SuTu":
        return 800 + (SuTu[10 - piece.position[0] - 1][piece.position[1] - 1] if name is "black" else SuTu[piece.position[0] - 1][8 - piece.position[1] - 1])
    elif piece.type is "Voi":
        return 1000 + (Voi[10 - piece.position[0] - 1][piece.position[1] - 1] if name is "black" else Voi[piece.position[0] - 1][8 - piece.position[1] - 1])

    return 0

class Player:
    # student do not allow to change two first functions
    def __init__(self, str_name):
        self.str = str_name ## Black or Red

    def __str__(self):
        return self.str

    # Student MUST implement this function
    # The return value should be a move that is denoted by:
        # piece: selected piece
        # (row, col): new position of selected piece
    def next_move(self, state):
        # state is Board(list_black, list_red)
        name = self.str

        a, b = calculateBestMove(state, name, 2, True)

        if name is "black":
            for x in state.list_black:
                if a.type == x.type:
                    return x, a.position
        else:
            for x in state.list_red:
                if a.type == x.type:
                    return x, a.position
