from main import Piece, Board
from copy import deepcopy
import random
INF = 9999

#generate all move for each piece
def generate_all_move(state, player):
    list_move = []
    list_occupied_cell = []
    vaild_list_move = []

    if player is 'black':
        for piece in state.list_black:
            list_occupied_cell.append((piece.type, piece.position))

        for piece in state.list_black:
            if piece.type is "SuTu" or piece.type is "Ho":
                list_move = list_move + SuTu_Ho_DiChuyen(state, player, piece)
            elif piece.type is "Voi" or piece.type is "Soi" or piece.type is "Cho" or piece.type is "Chuot":
                list_move = list_move + Voi_Soi_Cho_Chuot_DiChuyen(state, player, piece)
            elif piece.type is "Bao":
                list_move = list_move + Bao_DiChuyen(state, player, piece)
            elif piece.type is "Meo":
                list_move = list_move + Meo_DiChuyen(state, player, piece)

        for move in list_move:
            if move[1][0] <= 9 and move[1][0] >= 1 and move[1][1] <= 7 and move[1][1] >= 1:
                if (move[1] != cell[1] for cell in list_occupied_cell):
                    vaild_list_move.append(move)

        final_list_move = []
        final_list_move = list(set(vaild_list_move) - set(list_occupied_cell))

        return final_list_move

    elif player is 'red':
        for piece in state.list_red:
            list_occupied_cell.append((piece.type, piece.position))

        for piece in state.list_red:
            if piece.type is "SuTu" or piece.type is "Ho":
                list_move = list_move + SuTu_Ho_DiChuyen(state, player, piece)
            elif piece.type is "Voi" or piece.type is "Soi" or piece.type is "Cho" or piece.type is "Chuot":
                list_move = list_move + Voi_Soi_Cho_Chuot_DiChuyen(state, player, piece)
            elif piece.type is "Bao":
                list_move = list_move + Bao_DiChuyen(state, player, piece)
            elif piece.type is "Meo":
                list_move = list_move + Meo_DiChuyen(state, player, piece)            

        for move in list_move:
            if move[1][0] <= 9 and move[1][0] >= 1 and move[1][1] <= 7 and move[1][1] >= 1:
                if (move[1] != cell[1] for cell in list_occupied_cell):
                    vaild_list_move.append(move)

        final_list_move = []
        final_list_move = list(set(vaild_list_move) - set(list_occupied_cell))

        return final_list_move


#if a stronger b return true else false
def who_stronger(animal_a, animal_b):
    value = 0
    value1 = 0

    if animal_a is "Voi":
        value = 8
    elif animal_a is "SuTu":
        value = 7
    elif animal_a is "Ho":
        value = 6    
    elif animal_a is "Bao":
        value = 5
    elif animal_a is "Soi":
        value = 4
    elif animal_a is "Cho":
        value = 3
    elif animal_a is "Meo":
        value = 2
    elif animal_a is "Chuot":
        value = 1

    if animal_b is "Voi":
        value1 = 8
    elif animal_b is "SuTu":
        value1 = 7
    elif animal_b is "Ho":
        value1 = 6    
    elif animal_b is "Bao":
        value1 = 5
    elif animal_b is "Soi":
        value1 = 4
    elif animal_b is "Cho":
        value1 = 3
    elif animal_b is "Meo":
        value1 = 2
    elif animal_a is "Chuot":
        value1 = 1

    if animal_a is "Chuot" and animal_b is "Voi":
        return True
    elif value >= value1:
        return True
    else:
        return False

#move for animal
#postion (row, col)
def SuTu_Ho_DiChuyen(state, player, _piece):
    ''' define a move (move and jump) for SuTu, Ho: if can catch oppenent then catch immediately '''
    list_move = []
    type_animal = _piece.type

    jump1 = None
    jump2 = None
    jump3 = None
    jump4 = None
    jump5 = None
    jump6 = None

    if player is "black":
        for piece in state.list_black:
            if piece == _piece:
                turn1 = (piece.position[0] - 1, piece.position[1])
                turn2 = (piece.position[0] + 1, piece.position[1])
                turn3 = (piece.position[0], piece.position[1] + 1)
                turn4 = (piece.position[0], piece.position[1] - 1)

                list_move.extend([(type_animal, turn1), (type_animal, turn2), (type_animal, turn3), (type_animal, turn4)])

                if piece.position[0] == 7 and piece.position[1] >= 2 and piece.position[1] <= 3:
                    jump1 = (piece.position[0] - 4, piece.position[1])
                elif piece.position[0] >= 4 and piece.position[0] <= 6 and piece.position[1] == 1:
                    jump2 = (piece.position[0], piece.position[1] + 3)
                elif piece.position[0] >= 4 and piece.position[0] <= 6 and piece.position[1] == 4:
                    jump3 = (piece.position[0], piece.position[1] - 3)
                    jump4 = (piece.position[0], piece.position[1] + 3)
                elif piece.position[0] == 7 and piece.position[1] >= 5 and piece.position[1] <= 6:
                    jump5 = (piece.position[0] - 4, piece.position[1])
                elif piece.position[0] >= 4 and piece.position[0] <= 6 and piece.position[1] == 7:
                    jump6 = (piece.position[0], piece.position[1] - 3)

                list_jump = [jump1, jump2, jump3, jump4, jump5, jump6]
                for jump in list_jump:
                    if jump is not None:
                        list_move.append((type_animal, jump))
        
        list_opponent_cell = []
        for piece in state.list_red:
            list_opponent_cell.append((piece.type, piece.position))
        
        list_catch = []
        for move in list_move:
            for cell in list_opponent_cell:
                if move[1] == cell[1]:
                    if who_stronger(move[0], cell[1]):
                        list_catch.append(move)

        if len(list_catch) == 0:
            return list_move
        else:
            return list_catch

    elif player is "red":
        for piece in state.list_red:
            if piece == _piece:
                turn1 = (piece.position[0] - 1, piece.position[1])
                turn2 = (piece.position[0] + 1, piece.position[1])
                turn3 = (piece.position[0], piece.position[1] + 1)
                turn4 = (piece.position[0], piece.position[1] - 1) 

                list_move.extend([(type_animal, turn1), (type_animal, turn2), (type_animal, turn3), (type_animal, turn4)])

            if piece.position[0] == 3 and piece.position[1] >= 2 and piece.position[1] <= 3:
                jump1 = (piece.position[0] - 4, piece.position[1])
            elif piece.position[0] >= 4 and piece.position[0] <= 6 and piece.position[1] == 1:
                jump2 = (piece.position[0], piece.position[1] + 3)   
            elif piece.position[0] >= 4 and piece.position[0] <= 6 and piece.position[1] == 4:
                jump3 = (piece.position[0], piece.position[1] - 3)
                jump4 = (piece.position[0], piece.position[1] + 3)
            elif piece.position[0] == 3 and piece.position[1] >= 5 and piece.position[1] <= 6:
                jump5 = (piece.position[0] + 4, piece.position[1])
            elif piece.position[0] >= 4 and piece.position[0] <= 6 and piece.position[1] == 7:
                jump6 = (piece.position[0], piece.position[1] - 3)

            list_jump = [jump1, jump2, jump3, jump4, jump5, jump6]
            for jump in list_jump:
                if jump is not None:
                    list_move.append((type_animal, jump))
        
        list_opponent_cell = []
        for piece in state.list_black:
            list_opponent_cell.append((piece.type, piece.position))

        list_catch = []
        for move in list_move:
            for cell in list_opponent_cell:
                if move[1] == cell[1]:
                    if who_stronger(move[0], cell[1]):
                        list_catch.append(move)

        if len(list_catch) == 0:
            return list_move
        else:
            return list_catch                    

def Voi_Soi_Cho_Chuot_DiChuyen(state, player, _piece):
    ''' define a move for Voi, Soi, Cho, Chuot: if can catch oppenent then catch immediately  '''
    list_move = []
    type_animal = _piece.type

    if player is "black":
        for piece in state.list_black:
            if piece == _piece:
                turn1 = (piece.position[0] - 1, piece.position[1])
                turn2 = (piece.position[0] + 1, piece.position[1])
                turn3 = (piece.position[0], piece.position[1] + 1)
                turn4 = (piece.position[0], piece.position[1] - 1)  

        list_move.extend([(type_animal, turn1), (type_animal, turn2), (type_animal, turn3), (type_animal, turn4)])

        list_opponent_cell = []
        for piece in state.list_red:
            list_opponent_cell.append((piece.type, piece.position))
        
        list_catch = []
        for move in list_move:
            for cell in list_opponent_cell:
                if move[1] == cell[1]:
                    if who_stronger(move[0], cell[1]):
                        list_catch.append(move)

        if len(list_catch) == 0:
            return list_move
        else:
            return list_catch

    elif player is "red":
        for piece in state.list_red:
            if piece == _piece:
                turn1 = (piece.position[0] - 1, piece.position[1])
                turn2 = (piece.position[0] + 1, piece.position[1])
                turn3 = (piece.position[0], piece.position[1] + 1)
                turn4 = (piece.position[0], piece.position[1] - 1)            

        list_move.extend([(type_animal, turn1), (type_animal, turn2), (type_animal, turn3), (type_animal, turn4)])

        list_opponent_cell = []
        for piece in state.list_black:
            list_opponent_cell.append((piece.type, piece.position))
        
        list_catch = []
        for move in list_move:
            for cell in list_opponent_cell:
                if move[1] == cell[1]:
                    if who_stronger(move[0], cell[1]):
                        list_catch.append(move)

        if len(list_catch) == 0:
            return list_move
        else:
            return list_catch

def Bao_DiChuyen(state, player, _piece):
    ''' define a move for Bao: if can catch oppenent then catch immediately, can not swimming  '''
    list_move = []
    type_animal = _piece.type

    jump1 = None
    jump2 = None
    jump3 = None
    jump4 = None

    if player is "black":
        for piece in state.list_black:
            if piece == _piece:
                turn1 = (piece.position[0] - 1, piece.position[1])
                turn2 = (piece.position[0] + 1, piece.position[1])
                turn3 = (piece.position[0], piece.position[1] + 1)
                turn4 = (piece.position[0], piece.position[1] - 1)

                list_move.extend([(type_animal, turn1), (type_animal, turn2), (type_animal, turn3), (type_animal, turn4)])

                if piece.position[0] >= 4 and piece.position[0] <= 6 and piece.position[1] == 1:
                    jump1 = (piece.position[0], piece.position[1] + 3)
                elif piece.position[0] >= 4 and piece.position[0] <= 6 and piece.position[1] == 4:
                    jump2 = (piece.position[0], piece.position[1] - 3)
                    jump3 = (piece.position[0], piece.position[1] + 3)
                elif piece.position[0] >= 4 and piece.position[0] <= 6 and piece.position[1] == 7:
                    jump4 = (piece.position[0], piece.position[1] - 3)

                list_jump = [jump1, jump2, jump3, jump4]
                for jump in list_jump:
                    if jump is not None:
                        list_move.append((type_animal, jump))

        list_opponent_cell = []
        for piece in state.list_red:
            list_opponent_cell.append((piece.type, piece.position))
        
        list_catch = []
        for move in list_move:
            for cell in list_opponent_cell:
                if move[1] == cell[1]:
                    if who_stronger(move[0], cell[1]):
                        list_catch.append(move)

        if len(list_catch) == 0:
            return list_move
        else:
            return list_catch

    elif player is "red":
        for piece in state.list_red:
            if piece == _piece:
                turn1 = (piece.position[0] - 1, piece.position[1])
                turn2 = (piece.position[0] + 1, piece.position[1])
                turn3 = (piece.position[0], piece.position[1] + 1)
                turn4 = (piece.position[0], piece.position[1] - 1)

                list_move.extend([(type_animal, turn1), (type_animal, turn2), (type_animal, turn3), (type_animal, turn4)])

                if piece.position[0] >= 4 and piece.position[0] <= 6 and piece.position[1] == 1:
                    jump1 = (piece.position[0], piece.position[1] + 3)
                elif piece.position[0] >= 4 and piece.position[0] <= 6 and piece.position[1] == 4:
                    jump2 = (piece.position[0], piece.position[1] - 3)
                    jump3 = (piece.position[0], piece.position[1] + 3)
                elif piece.position[0] >= 4 and piece.position[0] <= 6 and piece.position[1] == 7:
                    jump4 = (piece.position[0], piece.position[1] - 3)

                list_jump = [jump1, jump2, jump3, jump4]
                for jump in list_jump:
                    if jump is not None:
                        list_move.append((type_animal, jump))

        list_opponent_cell = []
        for piece in state.list_black:
            list_opponent_cell.append((piece.type, piece.position))
        
        list_catch = []
        for move in list_move:
            for cell in list_opponent_cell:
                if move[1] == cell[1]:
                    if who_stronger(move[0], cell[1]):
                        list_catch.append(move)

        if len(list_catch) == 0:
            return list_move
        else:
            return list_catch        

def Meo_DiChuyen(state, player, _piece):
    ''' define move for Meo '''
    list_move = []
    type_animal = _piece.type

    if player is "black":
        for piece in state.list_black:
            if piece == _piece:
                turn1 = (piece.position[0] - 1, piece.position[1])
                turn2 = (piece.position[0] + 1, piece.position[1])
                turn3 = (piece.position[0], piece.position[1] + 1)
                turn4 = (piece.position[0], piece.position[1] - 1)

                list_move.extend([(type_animal, turn1), (type_animal, turn2), (type_animal, turn3), (type_animal, turn4)])

        list_opponent_cell = []
        for piece in state.list_red:
            list_opponent_cell.append((piece.type, piece.position))
        
        list_catch = []
        for move in list_move:
            for cell in list_opponent_cell:
                if move[1] == cell[1]:
                    if who_stronger(move[0], cell[1]):
                        list_catch.append(move)

        if len(list_catch) == 0:
            return list_move
        else:
            return list_catch        

    elif player is "red":
        for piece in state.list_red:
            if piece == _piece:
                turn1 = (piece.position[0] - 1, piece.position[1])
                turn2 = (piece.position[0] + 1, piece.position[1])
                turn3 = (piece.position[0], piece.position[1] + 1)
                turn4 = (piece.position[0], piece.position[1] - 1)
 
                list_move.extend([(type_animal, turn1), (type_animal, turn2), (type_animal, turn3), (type_animal, turn4)])

        list_opponent_cell = []
        for piece in state.list_black:
            list_opponent_cell.append((piece.type, piece.position))
        
        list_catch = []
        for move in list_move:
            for cell in list_opponent_cell:
                if move[1] == cell[1]:
                    if who_stronger(move[0], cell[1]):
                        list_catch.append(move)

        if len(list_catch) == 0:
            return list_move
        else:
            return list_catch  

            
#evaluate board score 
def evaluate_board(state):
    m_chuot = 500
    m_meo = 200
    m_soi = 300
    m_cho = 400
    m_bao = 500
    m_ho = 800
    m_sutu = 900
    m_voi = 1000
    # m_chuot = 0
    # m_meo = 0
    # m_soi = 0
    # m_cho = 0
    # m_bao = 0
    # m_ho = 0
    # m_sutu = 0
    # m_voi = 0

    Chuot = [[13,13,50,INF,50,13,11],\
             [13,13,13,50,13,12,11],\
             [13,13,13,13,11,11,10],\
             [13,12,12,11,9,9,8],\
             [12,12,12,11,9,9,8],\
             [11,12,12,10,9,9,8],\
             [10,10,10,9,8,8,8],\
             [9,9,9,9,8,8,8],\
             [8,8,8,0,8,8,8]]
    
    Meo = [[11,15,50,INF,50,15,11],\
            [11,11,15,50,15,11,11],\
            [10,11,11,15,11,11,10],\
            [8,0,0,10,0,0,10],\
            [8,0,0,8,0,0,10],\
            [8,0,0,8,0,0,10],\
            [8,8,8,8,10,10,10],\
            [8,8,8,8,8,10,13],\
            [8,8,8,0,8,8,8]]
    
    Soi = [[11,15,50,INF,50,15,11],\
            [10,11,15,50,15,11,10],\
            [9,19,11,15,11,10,9],\
            [9,0,0,10,0,0,9],\
            [8,0,0,8,0,0,8],\
            [8,0,0,8,0,0,8],\
            [8,8,8,8,10,8,8],\
            [8,8,8,8,13,12,8],\
            [8,8,8,0,12,12,8]]

    Cho = [[11,15,50,INF,50,15,11],\
            [10,11,15,50,15,11,10],\
            [9,10,11,15,11,10,9],\
            [9,0,0,10,0,0,9],\
            [8,0,0,8,0,0,8],\
            [8,0,0,8,0,0,8],\
            [8,8,8,8,8,8,8],\
            [8,10,13,8,8,8,8],\
            [8,12,12,0,8,8,8]]

    Bao = [[14,15,50,INF,50,15,14],\
            [13,14,15,50,15,14,13],\
            [13,13,14,15,14,13,13],\
            [12,0,0,15,0,0,12],\
            [11,0,0,14,0,0,11],\
            [10,0,0,13,0,0,10],\
            [9,9,10,10,9,9,9],\
            [9,9,9,9,9,9,9],\
            [9,9,9,0,9,9,9]]
    
    Ho = [[25,30,50,INF,50,30,25],\
            [25,25,30,50,30,25,25],\
            [18,20,20,30,20,20,18],\
            [15,0,0,15,0,0,15],\
            [15,0,0,15,0,0,15],\
            [15,0,0,15,0,0,15],\
            [14,16,16,14,16,16,14],\
            [12,12,12,12,12,12,12],\
            [10,12,12,0,12,12,10]]

    SuTu = [[25,30,50,INF,50,30,25],\
            [25,25,30,50,30,25,25],\
            [18,20,20,30,20,20,18],\
            [15,0,0,15,0,0,15],\
            [15,0,0,15,0,0,15],\
            [15,0,0,15,0,0,15],\
            [14,16,16,14,16,16,14],\
            [12,14,12,12,12,12,12],\
            [10,12,12,0,12,12,10]]
    
    Voi = [[25,30,50,INF,50,30,25],\
            [25,25,30,50,30,25,25],\
            [18,20,20,30,20,20,18],\
            [16,0,0,16,0,0,16],\
            [14,0,0,14,0,0,14],\
            [12,0,0,12,0,0,12],\
            [12,14,14,14,14,15,10],\
            [11,11,11,11,11,11,11],\
            [11,11,11,0,11,11,11]]


    value = 0

    for piece in state.list_black:
        if piece.type == "Chuot":
            value = m_chuot + value + Chuot[piece.position[0]-1][piece.position[1]-1]
        if piece.type == "Voi":
            value = m_voi + value + Voi[piece.position[0]-1][piece.position[1]-1]
        if piece.type == "SuTu":
            value = m_sutu + value + SuTu[piece.position[0]-1][piece.position[1]-1]
        if piece.type == "Ho":
            value = m_ho + value + Ho[piece.position[0]-1][piece.position[1]-1]
        if piece.type == "Bao":
            value = m_bao + value + Bao[piece.position[0]-1][piece.position[1]-1]
        if piece.type == "Cho":
            value = m_cho + value + Cho[piece.position[0]-1][piece.position[1]-1]
        if piece.type == "Soi":
            value = m_soi + value + Soi[piece.position[0]-1][piece.position[1]-1]
        if piece.type == "Meo":
            value = m_meo + value + Meo[piece.position[0]-1][piece.position[1]-1]

    for piece in state.list_red:
        if piece.type == "Chuot":
            value = value - m_chuot - Chuot[10-piece.position[0]-1][8-piece.position[1]-1]
        if piece.type == "Voi":
            value = value - m_voi - Voi[10-piece.position[0]-1][8-piece.position[1]-1]
        if piece.type == "SuTu":
            value = value - m_sutu - SuTu[10-piece.position[0]-1][8-piece.position[1]-1]
        if piece.type == "Ho":
            value = value - m_ho - Ho[10-piece.position[0]-1][8-piece.position[1]-1]
        if piece.type == "Bao":
            value = value - m_bao - Bao[10-piece.position[0]-1][8-piece.position[1]-1]
        if piece.type == "Cho":
            value = value - m_cho - Cho[10-piece.position[0]-1][8-piece.position[1]-1]
        if piece.type == "Soi":
            value = value - m_soi - Soi[10-piece.position[0]-1][8-piece.position[1]-1]
        if piece.type == "Meo":
            value = value - m_meo - Meo[10-piece.position[0]-1][8-piece.position[1]-1]

    return value

#change board stage
def change_board_state(state, move, player): 
    list_black = deepcopy(state.list_black)
    list_red = deepcopy(state.list_red)

    if player is 'black':
        for piece in list_black:
            if move[0] == piece.type:
                new_posistion = (move[1][0], move[1][1])
                piece.position = new_posistion

        for piece in list_red:
            if move[1] == piece.position:
                list_red.remove(piece)

    elif player is 'red':       
        for piece in list_red:
            if move[0] == piece.type:
                new_posistion = (move[1][0], move[1][1])
                piece.position = new_posistion

        for piece in list_black:
            if move[1] == piece.position:
                list_black.remove(piece)

    new_board = Board(list_black, list_red)
    return new_board

#minimax 
def minimax(state, depth, player, is_max = True):
    if depth == 0:
        value = evaluate_board(state)
        return [value, None]

    bestMove = None
    list_possible_move = generate_all_move(state, player)
    bestMoveValue = -INF if is_max == True else INF

    for move in list_possible_move:
        new_stage = change_board_state(state, move, player)
        value = minimax(new_stage, depth - 1, player, not is_max)[0]

        if is_max:
            if value > bestMoveValue:
                bestMoveValue = value
                bestMove = move
        else:
            if value < bestMoveValue:
                bestMoveValue = value
                bestMove = move

    return [bestMoveValue, bestMove]

#now, we upgrade algorithm with alpha beta pruning
def minimax_a_b(state, depth, player, alpha = -INF, beta = INF, is_Max = True):
    if depth == 0:
        value = evaluate_board(state)
        return [value, None]

    bestMove = None
    list_possible_move = generate_all_move(state, player)
    bestMoveValue = -INF if is_Max == True else INF

    for move in list_possible_move:
        new_stage = change_board_state(state, move, player)
        value = minimax_a_b(new_stage, depth - 1, player, alpha, beta, not is_Max)[0]

        if is_Max:
            if value > bestMoveValue:
                bestMoveValue = value
                bestMove = move
            
            alpha = max(alpha, value)
        else:
            if value < bestMoveValue:
                bestMoveValue = value
                bestMove = move
            
            beta = min(alpha, beta)
    
        if (beta <= alpha):
            break
        
    return [bestMoveValue, bestMove] 

# ======================== Class Player =======================================


class Player:
    # student do not allow to change two first functions
    def __init__(self, str_name):
        self.str = str_name

    def __str__(self):
        return self.str

    # Student MUST implement this function
    # The return value should be a move that is denoted by:
        # piece: selected piece
        # (row, col): new position of selected piece
        # ex :
        # piece = Piece('Voi', (7, 7))
        # new_pos = (6, 7)
        # return piece, new_pos
    def next_move(self, state):
        depth = 4
        player = self.str
        move = minimax_a_b(state, depth, player, alpha = -INF, beta = INF, is_Max = True)[1]
        type_animal = move[0]
        new_pos = move[1]
        
        selected_piece = Piece(None, None)
        
        if player is "black":
            for piece in state.list_black:
                if piece.type == type_animal:
                    selected_piece = piece
                    break
        else:
            for piece in state.list_red:
                if piece.type == type_animal:
                    selected_piece = piece
                    break

        return selected_piece, new_pos