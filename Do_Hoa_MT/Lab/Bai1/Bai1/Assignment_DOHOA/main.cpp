#include <windows.h>
#include <math.h>
#include <gl.h>
#include <glut.h>
#include <iostream>
#include <tchar.h>

#define PI			3.1415926
#define	COLORNUM		15

using namespace std;


float	ColorArr[COLORNUM][3] = {{0.9, 0.0, 0.0}, {0.0, 0.9, 0.0}, { 0.0,  0.0, 0.9}, 
								{0.9, 0.7,  0.0}, { 1.0, 0.0, 1.0},{ 0.0, 1.0, 1.0}, 
								 {0.3, 0.3, 0.3}, {0.4, 0.4, 0.4}, { 0.9,  0.9, 0.9},
								{1.0, 0.5,  0.5}, { 0.2, 0.6, 0.8},{ 0.0, 0.8, 0.9},
								{0.0, 0.0, 0.0}, {1.0, 1.0, 1.0}, {0.7, 0.0, 0.0}};


int		screenWidth = 600;
int		screenHeight= 600;

bool	bWireFrame = false;

float	base1Radius = 0.8;
float	base1Height = 0.2;
float	base1RotateStep = 5;

float	base2Radius = 0.6;
float	base2Height = 1.2;

float	cylinderRadius = 0.4;
float	cylinderHeight = 2.2;
float	cylinderRotateStep = 5;
float	cylinderTranslationStep = 0.05;
float	cylinderOffset = 0;
float	rotorOffset = 0;

float	ovanStep = 0.75*4/72;
float	ovanOffset = 0;

float	camera_angle;
float	camera_height;
float	camera_dis;
bool	animation = false;
bool	light = true;


/////////////



#pragma region support

class Point3
{
public:
	float x, y, z;
	void set(float dx, float dy, float dz)
						{ x = dx; y = dy; z = dz;}
	void set(Point3& p)
						{ x = p.x; y = p.y; z = p.z;}
	Point3() { x = y = z = 0;}
	Point3(float dx, float dy, float dz)
						{ x = dx; y = dy; z = dz;}

};
class Color3
{
public:
	float r, g, b;
	void set(float red, float green, float blue)
						{ r = red; g = green; b = blue;}
	void set(Color3& c)
						{ r = c.r; g = c.g; b = c.b;}
	Color3() { r = g = b = 0;}
	Color3(float red, float green, float blue)
						{ r = red; g = green; b = blue;}

};
class Point2
{
	public:
		Point2() { x = y = 0.0f; } // constructor 1
		Point2(float xx, float yy) { x = xx; y = yy; } // constructor 2
		void set(float xx, float yy) { x = xx; y = yy; }
		float getX() { return x;}
		float getY() { return y;}
		void draw()		{	glBegin(GL_POINTS);
								glVertex2f((GLfloat)x, (GLfloat)y);
							glEnd();
						}
	private:
		float 	x, y;
};
class IntRect
{
	 public:
		IntRect() { l = 0; r = 100; b = 0; t = 100; } // constructor
		IntRect( int left, int right, int bottom, int top)
			{ l = left; r = right; b = bottom; t = top; }
		void set( int left, int right, int bottom, int top)
			{ l = left; r = right; b = bottom; t = top; }
		void draw(){
						glRecti(l, b, r, t);
						glFlush();
					} // draw this rectangle using OpenGL
		int  getWidth(){return (r-l);}
		int  getHeight() { return (t-b);}
	 private:
		int	l, r, b, t;
};


class RealRect
{
	 public:
		RealRect() { l = 0; r = 100; b = 0; t = 100; } // constructor
		RealRect( float left, float right, float bottom, float top)
			{ l = left; r = right; b = bottom; t = top; }
		void set( float left, float right, float bottom, float top)
			{ l = left; r = right; b = bottom; t = top; }
		float  getWidth(){return (r-l);}
		float  getHeight() { return (t-b);}
		void RealRect::draw(){
							glRectf(l, b, r, t);
							glFlush();
						};// draw this rectangle using OpenGL
	 private:
		float	l, r, b, t;
};

class Vector3
{
public:
	float	x, y, z;
	void set(float dx, float dy, float dz)
						{ x = dx; y = dy; z = dz;}
	void set(Vector3& v)
						{ x = v.x; y = v.y; z = v.z;}
	void flip()
						{ x = -x; y = -y; z = -z;}
	void normalize();
	Vector3() { x = y = z = 0;}
	Vector3(float dx, float dy, float dz)
						{ x = dx; y = dy; z = dz;}
	Vector3(Vector3& v)
						{ x = v.x; y = v.y; z = v.z;}
	Vector3 cross(Vector3 b);
	float dot(Vector3 b);
};

Vector3 Vector3::cross(Vector3 b)
{
	Vector3 c(y*b.z-z*b.y, z*b.x-x*b.z, x*b.y-y*b.x);
	return c;
}
float Vector3::dot(Vector3 b)
{
	return x*b.x + y*b.y + z*b.z;
}
void Vector3::normalize()
{
	float temp = sqrt(x*x + y*y + z*z);
	x = x/temp;
	y = y/temp;
	z = z/temp;
}
#pragma endregion

#pragma region mesh
class VertexID
{
public:
	int		vertIndex;
	int		colorIndex;
};

class Face
{
public:
	int		nVerts;
	Vector3		facenorm;
	VertexID*	vert;
	
	Face()
	{
		nVerts	= 0;
		vert	= NULL;
	}
	~Face()
	{
		if(vert !=NULL)
		{
			delete[] vert;
			vert = NULL;
		}
		nVerts = 0;
	}
};

class Mesh
{
public:
	float slideX, slideY, slideZ;
	float rotateX, rotateY, rotateZ;
	float scaleX, scaleY, scaleZ;
	int		numVerts;
	Point3*		pt;
	
	int		numFaces;
	Face*		face;
public:
	Mesh()
	{
		numVerts	= 0;
		pt		= NULL;
		numFaces	= 0;
		face		= NULL;
		slideX = 0;
		slideY = 0;
		slideZ = 0;
		rotateX = 0;
		rotateY = 0;
		rotateZ = 0;
		scaleX = 0;
		scaleY = 0;
		scaleZ = 0;
	}
	~Mesh()
	{
		if (pt != NULL)
		{
			delete[] pt;
		}	
		if(face != NULL)
		{
			delete[] face;
		}
		numVerts = 0;
		numFaces = 0;
		slideX = 0;
		slideY = 0;
		slideZ = 0;
		rotateX = 0;
		rotateY = 0;
		rotateZ = 0;
		scaleX = 0;
		scaleY = 0;
		scaleZ = 0;
	}

	void Draw();
	void CalculateFacesNorm();
	void DrawWireframe();
	void DrawColor();
	int SetColor(int colorIdx);

	void CreateTetrahedron();
	void CreateDeGia(float khoangcach);
	void CreateCuboid(float	fSizeX, float fSizeY, float	fSizeZ);
	void CreateTR(float	R,float H);
	void CreateOvan(float x, float y, float r, float h);
	void CreateTruVer2(float r1, float h1, float r2, float h2, float l);
	void CreateBanNguyet(float x, float y, float r, float h);

	void CreateHinhThoi(float R);

};




void Mesh::CalculateFacesNorm(){
	for (int i = 0; i < numFaces; i++){
		for (int j = 0; j < face[i].nVerts - 1; j++){
			face[i].facenorm.x += (pt[face[i].vert[j].vertIndex].y-pt[face[i].vert[j+1].vertIndex].y)*(pt[face[i].vert[j].vertIndex].z+pt[face[i].vert[j+1].vertIndex].z); 
			face[i].facenorm.y += (pt[face[i].vert[j].vertIndex].z-pt[face[i].vert[j+1].vertIndex].z)*(pt[face[i].vert[j].vertIndex].x+pt[face[i].vert[j+1].vertIndex].x); 
			face[i].facenorm.z += (pt[face[i].vert[j].vertIndex].x-pt[face[i].vert[j+1].vertIndex].x)*(pt[face[i].vert[j].vertIndex].y+pt[face[i].vert[j+1].vertIndex].y); 
		
		}
		cout << face[i].facenorm.x << endl;
	}

}

int Mesh::SetColor(int colorIdx){
	for (int f = 0; f < numFaces; f++){
		for (int v = 0; v < face[f].nVerts; v++){
			face[f].vert[v].colorIndex = colorIdx;
		}
	}
	return 0;
}


void Mesh::CreateDeGia(float khoangcach)
{
	int i;

	numVerts=8;
	pt = new Point3[numVerts];
	// z la 
	float x, y, z;
	x = 0.5; y = 0.1; z = 0.2;
	pt[0].set( -z + khoangcach, y, x);
	pt[1].set( z + khoangcach, y, x);
	pt[2].set( z + khoangcach, y, -x);
	pt[3].set(-z + khoangcach, y, -x);
	pt[4].set(-z + khoangcach, 0, x);
	pt[5].set( z + khoangcach, 0, x);
	pt[6].set( z + khoangcach, 0, -x);
	pt[7].set(-z + khoangcach, 0, -x);


	numFaces= 6;
	face = new Face[numFaces];

	//Left face
	face[0].nVerts = 4;
	face[0].vert = new VertexID[face[0].nVerts];
	face[0].vert[0].vertIndex = 1;
	face[0].vert[1].vertIndex = 5;
	face[0].vert[2].vertIndex = 6;
	face[0].vert[3].vertIndex = 2;
	
	//Right face
	face[1].nVerts = 4;
	face[1].vert = new VertexID[face[1].nVerts];
	face[1].vert[0].vertIndex = 0;
	face[1].vert[1].vertIndex = 3;
	face[1].vert[2].vertIndex = 7;
	face[1].vert[3].vertIndex = 4;

	//top face
	face[2].nVerts = 4;
	face[2].vert = new VertexID[face[2].nVerts];
	face[2].vert[0].vertIndex = 0;
	face[2].vert[1].vertIndex = 1;
	face[2].vert[2].vertIndex = 2;
	face[2].vert[3].vertIndex = 3;

	//bottom face
	face[3].nVerts = 4;
	face[3].vert = new VertexID[face[3].nVerts];
	face[3].vert[0].vertIndex = 7;
	face[3].vert[1].vertIndex = 6;
	face[3].vert[2].vertIndex = 5;
	face[3].vert[3].vertIndex = 4;
	/*for(i = 0; i<face[3].nVerts ; i++)
		face[3].vert[i].colorIndex = 3;*/

	//near face
	face[4].nVerts = 4;
	face[4].vert = new VertexID[face[4].nVerts];
	face[4].vert[0].vertIndex = 4;
	face[4].vert[1].vertIndex = 5;
	face[4].vert[2].vertIndex = 1;
	face[4].vert[3].vertIndex = 0;
	/*for(i = 0; i<face[4].nVerts ; i++)
		face[4].vert[i].colorIndex = 4;*/

	//Far face
	face[5].nVerts = 4;
	face[5].vert = new VertexID[face[5].nVerts];
	face[5].vert[0].vertIndex = 3;
	face[5].vert[1].vertIndex = 2;
	face[5].vert[2].vertIndex = 6;
	face[5].vert[3].vertIndex = 7;
	/*for(i = 0; i<face[5].nVerts ; i++)
		face[5].vert[i].colorIndex = 5;*/

}

void Mesh::CreateHinhThoi(float R){
	const int N = 6;
	float x[N], y[N];
	numVerts = N;
	pt = new Point3[numVerts+1];
	pt[0].set(0, 0, 0);
	int theta = -90;
	// Tim toa do cac dinh
	for (int i = 0; i < N; i++){
		x[i] = R*cos(theta*3.14/180);
		y[i] = R*sin(theta*3.14/180);
		theta += 360/N;
	}

	for(int i = 0; i < numVerts;i++){
		pt[i+1].set(x[i], 0, y[i]);
	}

	numFaces = 3;
	face = new Face[numFaces];

	face[0].nVerts = 4;
	face[0].vert = new VertexID[4];
	face[0].vert[0].vertIndex = 0;
	face[0].vert[1].vertIndex = 1;
	face[0].vert[2].vertIndex = 2;
	face[0].vert[3].vertIndex = 3;
	for(int i = 0; i<face[0].nVerts ; i++)
		face[0].vert[i].colorIndex = 13;

	face[1].nVerts = 4;
	face[1].vert = new VertexID[4];
	face[1].vert[0].vertIndex = 0;
	face[1].vert[1].vertIndex = 3;
	face[1].vert[2].vertIndex = 4;
	face[1].vert[3].vertIndex = 5;
	for(int i = 0; i<face[0].nVerts ; i++)
		face[1].vert[i].colorIndex = 11;

	face[2].nVerts = 4;
	face[2].vert = new VertexID[4];
	face[2].vert[0].vertIndex = 0;
	face[2].vert[1].vertIndex = 5;
	face[2].vert[2].vertIndex = 6;
	face[2].vert[3].vertIndex = 1;
	for(int i = 0; i<face[0].nVerts ; i++)
		face[2].vert[i].colorIndex = 10;
}



void Mesh::CreateTetrahedron()
{
	int i;
	numVerts=4;
	pt = new Point3[numVerts];
	pt[0].set(0, 0, 0);
	pt[1].set(1, 0, 0);
	pt[2].set(0, 1, 0);
	pt[3].set(0, 0, 1);

	numFaces= 4;
	face = new Face[numFaces];

	face[0].nVerts = 3;
	face[0].vert = new VertexID[face[0].nVerts];
	face[0].vert[0].vertIndex = 1;
	face[0].vert[1].vertIndex = 2;
	face[0].vert[2].vertIndex = 3;
	for(i = 0; i<face[0].nVerts ; i++)
		face[0].vert[i].colorIndex = 0;
	

	face[1].nVerts = 3;
	face[1].vert = new VertexID[face[1].nVerts];
	face[1].vert[0].vertIndex = 0;	
	face[1].vert[1].vertIndex = 2;
	face[1].vert[2].vertIndex = 1;
	for(i = 0; i<face[1].nVerts ; i++)
		face[1].vert[i].colorIndex = 1;

	
	face[2].nVerts = 3;
	face[2].vert = new VertexID[face[2].nVerts];
	face[2].vert[0].vertIndex = 0;
	face[2].vert[1].vertIndex = 3;
	face[2].vert[2].vertIndex = 2;
	for(i = 0; i<face[2].nVerts ; i++)
		face[2].vert[i].colorIndex = 2;


	face[3].nVerts = 3;
	face[3].vert = new VertexID[face[3].nVerts];
	face[3].vert[0].vertIndex = 1;
	face[3].vert[1].vertIndex = 3;
	face[3].vert[2].vertIndex = 0;
	for(i = 0; i<face[3].nVerts ; i++)
		face[3].vert[i].colorIndex = 3;
}

void Mesh::Draw() {
	for (int f = 0; f < numFaces; f++){
		glBegin(GL_POLYGON);
		for (int v = 0; v < face[f].nVerts; v++){
			int		iv = face[f].vert[v].vertIndex;
			glNormal3f(face[f].facenorm.x, face[f].facenorm.y, face[f].facenorm.z);
			glVertex3f(pt[iv].x, pt[iv].y, pt[iv].z);
		}
		glEnd();
	}
}


void Mesh::DrawWireframe()
{
	glColor3f(0, 0, 0);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	for (int f = 0; f < numFaces; f++)
	{
		glBegin(GL_POLYGON);
		for (int v = 0; v < face[f].nVerts; v++)
		{
			int		iv = face[f].vert[v].vertIndex;

			glVertex3f(pt[iv].x, pt[iv].y, pt[iv].z);
		}
		glEnd();
	}
	// them vao
}

void Mesh::DrawColor()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	for (int f = 0; f < numFaces; f++)
	{
		glBegin(GL_POLYGON);
		for (int v = 0; v < face[f].nVerts; v++)
		{
			int		iv = face[f].vert[v].vertIndex;
			int		ic = face[f].vert[v].colorIndex;
			
			//ic = f % COLORNUM;

			glColor3f(ColorArr[ic][0], ColorArr[ic][1], ColorArr[ic][2]); 
			glVertex3f(pt[iv].x, pt[iv].y, pt[iv].z);
		}
		glEnd();
	}
}

void Mesh::CreateCuboid(float	fSizeX, float fSizeY, float	fSizeZ){
	int i;

	numVerts=8;
	pt = new Point3[numVerts];
	pt[0].set(-fSizeX, fSizeY, fSizeZ);
	pt[1].set( fSizeX, fSizeY, fSizeZ);
	pt[2].set( fSizeX, fSizeY, -fSizeZ);
	pt[3].set(-fSizeX, fSizeY, -fSizeZ);
	pt[4].set(-fSizeX, -fSizeY, fSizeZ);
	pt[5].set( fSizeX, -fSizeY, fSizeZ);
	pt[6].set( fSizeX, -fSizeY, -fSizeZ);
	pt[7].set(-fSizeX, -fSizeY, -fSizeZ);


	numFaces= 6;
	face = new Face[numFaces];

	//Left face
	face[0].nVerts = 4;
	face[0].vert = new VertexID[face[0].nVerts];
	face[0].vert[0].vertIndex = 1;
	face[0].vert[1].vertIndex = 5;
	face[0].vert[2].vertIndex = 6;
	face[0].vert[3].vertIndex = 2;
	
	//Right face
	face[1].nVerts = 4;
	face[1].vert = new VertexID[face[1].nVerts];
	face[1].vert[0].vertIndex = 0;
	face[1].vert[1].vertIndex = 3;
	face[1].vert[2].vertIndex = 7;
	face[1].vert[3].vertIndex = 4;

	//top face
	face[2].nVerts = 4;
	face[2].vert = new VertexID[face[2].nVerts];
	face[2].vert[0].vertIndex = 0;
	face[2].vert[1].vertIndex = 1;
	face[2].vert[2].vertIndex = 2;
	face[2].vert[3].vertIndex = 3;

	//bottom face
	face[3].nVerts = 4;
	face[3].vert = new VertexID[face[3].nVerts];
	face[3].vert[0].vertIndex = 7;
	face[3].vert[1].vertIndex = 6;
	face[3].vert[2].vertIndex = 5;
	face[3].vert[3].vertIndex = 4;

	//near face
	face[4].nVerts = 4;
	face[4].vert = new VertexID[face[4].nVerts];
	face[4].vert[0].vertIndex = 4;
	face[4].vert[1].vertIndex = 5;
	face[4].vert[2].vertIndex = 1;
	face[4].vert[3].vertIndex = 0;

	//Far face
	face[5].nVerts = 4;
	face[5].vert = new VertexID[face[5].nVerts];
	face[5].vert[0].vertIndex = 3;
	face[5].vert[1].vertIndex = 2;
	face[5].vert[2].vertIndex = 6;
	face[5].vert[3].vertIndex = 7;

}

void Mesh::CreateTR(float R,float H){
	const int N = 20;
	float x[N], y[N];
	
	numVerts = N;
	pt = new Point3[numVerts*2 + 2];
	pt[0].set(0,0,0);
	pt[1].set(0,H,0);
	int theta = 0;
	// Tim toa do cac dinh
	for (int i = 0; i < N; i++){
		x[i] = R*cos(theta*3.14/180);
		y[i] = R*sin(theta*3.14/180);
		theta += 360/N;
	}

	for(int i = 0; i < numVerts;i++){
		pt[i+2].set(x[i], 0, y[i]);
	}

	for(int i = numVerts; i < numVerts*2; i++){
		pt[i+2].set(x[i-numVerts], H, y[i-numVerts]);
	}

	numFaces = N*3;
	face = new Face[numFaces];

	// Draw cicle bottom
	for (int i = 0; i < N; i++){
		face[i].nVerts = 3;
		face[i].vert = new VertexID[face[i].nVerts];
		face[i].vert[0].vertIndex = 0;
		face[i].vert[1].vertIndex = i+2;
		if (i == N-1){
			face[i].vert[2].vertIndex = 2;
		}
		else{
			face[i].vert[2].vertIndex = i+3;
		}
		
	}

	// Draw cicle top
	for (int i = N; i < N*2; i++){
		face[i].nVerts = 3;
		face[i].vert = new VertexID[face[i].nVerts];
		face[i].vert[0].vertIndex = 1;
		face[i].vert[1].vertIndex = i+2;
		if (i == N*2-1){
			face[i].vert[2].vertIndex = N+2;
		}
		else{
			face[i].vert[2].vertIndex = i+3;
		}
	}

	// Draw arround
	for (int i = N*2; i < N*3; i++){
		face[i].nVerts = 4;
		face[i].vert = new VertexID[face[i].nVerts];
		face[i].vert[0].vertIndex = i - N*2 + 2;
		face[i].vert[3].vertIndex = i - N + 2;
		if (i == N*3 - 1){
			face[i].vert[1].vertIndex = 2;
			face[i].vert[2].vertIndex = N + 2;
		}
		else{
			face[i].vert[1].vertIndex = i - N*2 + 3;
			face[i].vert[2].vertIndex = i - N + 3;
		}
		
	}
}

void Mesh::CreateTruVer2(float r1, float h1, float r2, float h2, float l){
	const int N = 20;
	// x1 toa do duoi, y1 toa do tren, x2, y2 hinh nho
	float x1[N], y1[N], x2[N], y2[N];
	
	numVerts = N;
	pt = new Point3[numVerts*4 + 2];
	pt[0].set(0,0,0);
	pt[1].set(0,h1+h2+l,0);
	int theta = 0;
	// Tim toa do cac dinh
	for (int i = 0; i < N; i++){
		x1[i] = r1*cos(theta*3.14/180);
		y1[i] = r1*sin(theta*3.14/180);
		theta += 360/N;
	}
	theta = 0;
	for (int i = 0; i < N; i++){
		x2[i] = r2*cos(theta*3.14/180);
		y2[i] = r2*sin(theta*3.14/180);
		theta += 360/N;
	}

	// Set vert
	// Vert bottom
	for(int i = 0; i < numVerts;i++){
		pt[i+2].set(x1[i], 0, y1[i]);
	}
	// Vert middle 1
	for(int i = numVerts; i < numVerts*2; i++){
		pt[i+2].set(x1[i-numVerts], h1, y1[i-numVerts]);
	}
	// Vert middle 2
	for(int i = numVerts*2; i < numVerts*3; i++){
		pt[i+2].set(x2[i-numVerts*2], h1 + l, y2[i-numVerts*2]); // cong them l
	}
	// Vert top
	for(int i = numVerts*3; i < numVerts*4; i++){
		pt[i+2].set(x2[i-numVerts*3], h1+h2 + l, y2[i-numVerts*3]); // cong them l
	}

	
	numFaces = N*5;
	face = new Face[numFaces];
	// Draw cicle bottom
	for (int i = 0; i < N; i++){
		face[i].nVerts = 3;
		face[i].vert = new VertexID[face[i].nVerts];
		face[i].vert[0].vertIndex = 0;
		face[i].vert[1].vertIndex = i+2;
		if (i == N-1){
			face[i].vert[2].vertIndex = 2;
		}
		else{
			face[i].vert[2].vertIndex = i+3;
		}
		
	}
	// Draw middle
	for (int i = N; i < N*2; i++){
		face[i].nVerts = 4;
		face[i].vert = new VertexID[face[i].nVerts];
		face[i].vert[0].vertIndex = i+2;
		face[i].vert[3].vertIndex = i+N+2;
		if (i == N*2-1){
			face[i].vert[1].vertIndex = N+2;
			face[i].vert[2].vertIndex = N*2+2;
		}
		else{
			face[i].vert[1].vertIndex = i+3;
			face[i].vert[2].vertIndex = i+N+3;
		}
	}
	// Draw cicle top
	for (int i = 0; i < N; i++){
		face[i+N*2].nVerts = 3;
		face[i+N*2].vert = new VertexID[face[i+N*2].nVerts];
		face[i+N*2].vert[0].vertIndex = 1;
		face[i+N*2].vert[1].vertIndex = i+N*3+2;
		if (i == N-1){
			face[i+N*2].vert[2].vertIndex = N*3+2;
		}
		else{
			face[i+N*2].vert[2].vertIndex = i+N*3+3;
		}
		
	}
	// Draw arround bottom
	for (int i = 0; i < N; i++){
		face[i+N*3].nVerts = 4;
		face[i+N*3].vert = new VertexID[face[i+N*3].nVerts];
		face[i+N*3].vert[0].vertIndex = i + 2;
		face[i+N*3].vert[3].vertIndex = i + N + 2;
		if (i == N - 1){
			face[i+N*3].vert[1].vertIndex = 3;
			face[i+N*3].vert[2].vertIndex = N + 3;
		}
		else{
			face[i+N*3].vert[1].vertIndex = i + 3;
			face[i+N*3].vert[2].vertIndex = i + N + 3;
		}
		
	}
	// Draw arround top
	for (int i = 0; i < N; i++){
		face[i+N*4].nVerts = 4;
		face[i+N*4].vert = new VertexID[face[i+N*4].nVerts];
		face[i+N*4].vert[0].vertIndex = i + N*2 + 2;
		face[i+N*4].vert[3].vertIndex = i + N*3 + 2;
		if (i == N - 1){
			face[i+N*4].vert[1].vertIndex = 3 + N*2;
			face[i+N*4].vert[2].vertIndex = N*3 + 3;
		}
		else{
			face[i+N*4].vert[1].vertIndex = i + N*2 + 3;
			face[i+N*4].vert[2].vertIndex = i + N*3 + 3;
		}
		
	}
}

void Mesh::CreateOvan(float x, float y, float r, float h){
	const int N = 19;
	// x1 toa do duoi -x, y1 toa do tren -x, x2, y2 nguoc lai
	float x1[N], y1[N], x2[N], y2[N];
	
	numVerts = N;
	pt = new Point3[numVerts*8];
	int theta = 90;
	// Tim toa do cac dinh
	for (int i = 0; i < N; i++){
		x1[i] = r*cos(theta*3.14/180) ;
		y1[i] = r*sin(theta*3.14/180) ;
		theta += 10;
	}
	theta = 90;
	for (int i = 0; i < N; i++){
		x2[i] = (x)*cos(theta*3.14/180);
		y2[i] = (x)*sin(theta*3.14/180);
		theta += 10;
	}

	// Set vert
	// Vert bottom left
	for(int i = 0; i < numVerts;i++){
		pt[i].set(x1[i] - y/2.0, 0, y1[i]);
	}
	for(int i = numVerts; i < numVerts*2;i++){
		pt[i].set(x2[i-numVerts] - y/2.0, 0, y2[i-numVerts]);
	}
	// Vert top left
	for(int i = numVerts*2; i < numVerts*3;i++){
		pt[i].set(x1[i-numVerts*2] - y/2.0, h, y1[i-numVerts*2]);
	}
	for(int i = numVerts*3; i < numVerts*4;i++){
		pt[i].set(x2[i-numVerts*3] - y/2.0, h, y2[i-numVerts*3]);
	}
	// Set Vert Right
	for(int i = numVerts*4; i < numVerts*5;i++){
		pt[i].set(-x1[i-numVerts*4] + y/2.0, 0, y1[i-numVerts*4]);
	}
	for(int i = numVerts*5; i < numVerts*6;i++){
		pt[i].set(-x2[i-numVerts*5] + y/2.0, 0, y2[i-numVerts*5]);
	}
	for(int i = numVerts*6; i < numVerts*7;i++){
		pt[i].set(-x1[i-numVerts*6] + y/2.0, h, y1[i-numVerts*6]);
	}
	for(int i = numVerts*7; i < numVerts*8;i++){
		pt[i].set(-x2[i-numVerts*7] + y/2.0, h, y2[i-numVerts*7]);
	}


	numFaces = N*8;
	face = new Face[numFaces];
	//////////// Draw phia cuc ben trai
	// Draw cicle bottom
	for (int i = 0; i < N - 1; i++){
		// Day trai Xanh duong
		face[i].nVerts = 4;
		face[i].vert = new VertexID[face[i].nVerts];
		face[i].vert[0].vertIndex = i;
		face[i].vert[1].vertIndex = i+1;
		face[i].vert[2].vertIndex = i+N+1;
		face[i].vert[3].vertIndex = i+N;
		
		/*for(int j = 0; j<face[i].nVerts ; j++)
			face[i].vert[j].colorIndex = i;*/
	}
	// Phia tren ben trai
	for (int i = N; i < N*2-1;i++){
		// Phia tren ben trai
		face[i - 1].nVerts = 4;
		face[i - 1].vert = new VertexID[face[i - 1].nVerts];
		face[i - 1].vert[0].vertIndex = i+N;
		face[i - 1].vert[1].vertIndex = i+N+1;
		face[i - 1].vert[2].vertIndex = i+N*2+1;
		face[i - 1].vert[3].vertIndex = i+N*2;
		
	}
	// Phia sau ben trai
	for (int i = N*2; i < N*3-1;i++){
		face[i - 2].nVerts = 4;
		face[i - 2].vert = new VertexID[face[i - 2].nVerts];
		face[i - 2].vert[0].vertIndex = i-N*2;
		face[i - 2].vert[1].vertIndex = i-N*2+1;
		face[i - 2].vert[2].vertIndex = i+1;
		face[i - 2].vert[3].vertIndex = i;
		
	}
	// Phia truoc ben trai
	for (int i = N*3; i < N*4-1;i++){
		face[i - 3].nVerts = 4;
		face[i - 3].vert = new VertexID[face[i - 3].nVerts];
		face[i - 3].vert[0].vertIndex = i-N*2;
		face[i - 3].vert[1].vertIndex = i-N*2+1;
		face[i - 3].vert[2].vertIndex = i+1;
		face[i - 3].vert[3].vertIndex = i;
		
	}

	//////////// Draw phia cuc ben phai
	// Draw cicle bottom
	for (int i = N*4; i < N*5 - 1; i++){
		face[i - 4].nVerts = 4;
		face[i - 4].vert = new VertexID[face[i - 4].nVerts];
		face[i - 4].vert[0].vertIndex = i;
		face[i - 4].vert[1].vertIndex = i+1;
		face[i - 4].vert[2].vertIndex = i+N+1;
		face[i - 4].vert[3].vertIndex = i+N;
		
	}
	for (int i = N*5; i < N*6-1;i++){
		// Phia tren ben phai
		face[i - 5].nVerts = 4;
		face[i - 5].vert = new VertexID[face[i - 5].nVerts];
		face[i - 5].vert[0].vertIndex = i+N;
		face[i - 5].vert[1].vertIndex = i+N+1;
		face[i - 5].vert[2].vertIndex = i+N*2+1;
		face[i - 5].vert[3].vertIndex = i+N*2;
		
	}
	// Phia sau ben phai
	for (int i = N*6; i < N*7-1;i++){
		face[i - 6].nVerts = 4;
		face[i - 6].vert = new VertexID[face[i - 6].nVerts];
		face[i - 6].vert[0].vertIndex = i-N*2;
		face[i - 6].vert[1].vertIndex = i-N*2+1;
		face[i - 6].vert[2].vertIndex = i+1;
		face[i - 6].vert[3].vertIndex = i;
		
	}
	// Phia truoc ben phai
	for (int i = N*7; i < N*8-1;i++){
		face[i - 7].nVerts = 4;
		face[i - 7].vert = new VertexID[face[i - 7].nVerts];
		face[i - 7].vert[0].vertIndex = i-N*2;
		face[i - 7].vert[1].vertIndex = i-N*2+1;
		face[i - 7].vert[2].vertIndex = i+1;
		face[i - 7].vert[3].vertIndex = i;
		
	}

	int k = N*8-8;
	// Hinh hop o giua
	face[k].nVerts = 4;
	face[k].vert = new VertexID[face[k].nVerts];
	face[k].vert[0].vertIndex = 0;
	face[k].vert[1].vertIndex = N*2;
	face[k].vert[2].vertIndex = N*6;
	face[k].vert[3].vertIndex = N*4;


	face[k+1].nVerts = 4;
	face[k+1].vert = new VertexID[face[k+1].nVerts];
	face[k+1].vert[0].vertIndex = N;
	face[k+1].vert[1].vertIndex = N*3;
	face[k+1].vert[2].vertIndex = N*7;
	face[k+1].vert[3].vertIndex = N*5;


	face[k+2].nVerts = 4;
	face[k+2].vert = new VertexID[face[k+2].nVerts];
	face[k+2].vert[0].vertIndex = 0;
	face[k+2].vert[1].vertIndex = N;
	face[k+2].vert[2].vertIndex = N*5;
	face[k+2].vert[3].vertIndex = N*4;


	face[k+3].nVerts = 4;
	face[k+3].vert = new VertexID[face[k+3].nVerts];
	face[k+3].vert[0].vertIndex = N*2;
	face[k+3].vert[1].vertIndex = N*3;
	face[k+3].vert[2].vertIndex = N*7;
	face[k+3].vert[3].vertIndex = N*6;

	k += 4;
	// Hinh hop o giua
	face[k].nVerts = 4;
	face[k].vert = new VertexID[face[k].nVerts];
	face[k].vert[0].vertIndex = N-1;
	face[k].vert[1].vertIndex = N*2-1;
	face[k].vert[2].vertIndex = N*6-1;
	face[k].vert[3].vertIndex = N*5-1;


	face[k+1].nVerts = 4;
	face[k+1].vert = new VertexID[face[k+1].nVerts];
	face[k+1].vert[0].vertIndex = N*2-1;
	face[k+1].vert[1].vertIndex = N*4-1;
	face[k+1].vert[2].vertIndex = N*8-1;
	face[k+1].vert[3].vertIndex = N*6-1;


	face[k+2].nVerts = 4;
	face[k+2].vert = new VertexID[face[k+2].nVerts];
	face[k+2].vert[0].vertIndex = N*3-1;
	face[k+2].vert[1].vertIndex = N*4-1;
	face[k+2].vert[2].vertIndex = N*8-1;
	face[k+2].vert[3].vertIndex = N*7-1;


	face[k+3].nVerts = 4;
	face[k+3].vert = new VertexID[face[k+3].nVerts];
	face[k+3].vert[0].vertIndex = N-1;
	face[k+3].vert[1].vertIndex = N*3-1;
	face[k+3].vert[2].vertIndex = N*7-1;
	face[k+3].vert[3].vertIndex = N*5-1;

}

void Mesh::CreateBanNguyet(float x, float y, float r, float h){
	const int N = 19;
	float nho = (x - r*2)/2.0;
	float hTam = h - r - nho;
	float a[N], b[N], c[N], d[N], e[N], f[N];
	
	numVerts = N;
	pt = new Point3[numVerts*8+4];
	pt[numVerts*8-1].set(0,hTam, 0);
	int theta = 0;
	// Tim toa do cac dinh (a,b) vanh tren, (c,d) vanh duoi, (e,f) vanh lon.
	float khoangcach = 0;
	for (int i = 0; i < N; i++){
		a[i] = r*cos(theta*3.14/180) + khoangcach;
		b[i] = r*sin(theta*3.14/180);
		theta += 10;
	}
	theta = 180;
	for (int i = 0; i < N; i++){
		c[i] = r*cos(theta*3.14/180) + khoangcach;
		d[i] = r*sin(theta*3.14/180);
		theta += 10;
	}
	theta = 0;
	for (int i = 0; i < N; i++){
		e[i] = (r+nho)*cos(theta*3.14/180) + khoangcach;
		f[i] = (r+nho)*sin(theta*3.14/180);
		theta += 10;
	}

	// Gan vao pt: Nho tren -> nho duoi -> bu tren
	for(int i = 0; i < numVerts;i++){
		pt[i].set(a[i], b[i]+hTam, 0);
	}
	for(int i = numVerts; i < numVerts*2;i++){
		pt[i].set(c[i-numVerts], d[i-numVerts]+hTam, 0);
	}
	for(int i = numVerts*2; i < numVerts*3;i++){
		pt[i].set(e[i-numVerts*2], f[i-numVerts*2]+hTam, 0);
	}
	for(int i = numVerts*3; i < numVerts*4;i++){
		pt[i].set(a[i-numVerts*3], b[i-numVerts*3]+hTam, y);
	}
	for(int i = numVerts*4; i < numVerts*5;i++){
		pt[i].set(c[i-numVerts*4], d[i-numVerts*4]+hTam, y);
	}
	for(int i = numVerts*5; i < numVerts*6;i++){
		pt[i].set(e[i-numVerts*5], f[i-numVerts*5]+hTam, y);
	}
	for(int i = numVerts*6; i < numVerts*7;i++){ 
		pt[i].set(a[i-numVerts*6], 0, 0);
	}
	for(int i = numVerts*7; i < numVerts*8;i++){ 
		pt[i].set(a[i-numVerts*7], 0, y);
	}
	// Set 4 coner
	pt[numVerts*8].set(e[0],0,0);
	pt[numVerts*8+1].set(e[numVerts-1],0,0);
	pt[numVerts*8+2].set(e[0],0,y);
	pt[numVerts*8+3].set(e[numVerts-1],0,y);

	numFaces = N*8;
	face = new Face[numFaces];

	// Draw cicle bottom
	for (int i = 0; i < N-1; i++){
		face[i].nVerts = 4;
		face[i].vert = new VertexID[face[i].nVerts];
		face[i].vert[0].vertIndex = i;
		face[i].vert[1].vertIndex = i+1;
		face[i].vert[2].vertIndex = i+N*2+1;
		face[i].vert[3].vertIndex = i+N*2;
		
	}

	for (int i = 0; i < N-1; i++){
		face[i+N-1].nVerts = 4;
		face[i+N-1].vert = new VertexID[face[i+N-1].nVerts];
		face[i+N-1].vert[0].vertIndex = i+N*3;
		face[i+N-1].vert[1].vertIndex = i+N*3+1;
		face[i+N-1].vert[2].vertIndex = i+N*5+1;
		face[i+N-1].vert[3].vertIndex = i+N*5;
		
	}
	// Draw top top
	for (int i = 0; i < N-1; i++){
		face[i+N*2-2].nVerts = 4;
		face[i+N*2-2].vert = new VertexID[face[i+N*2-2].nVerts];
		face[i+N*2-2].vert[0].vertIndex = i+N*2;
		face[i+N*2-2].vert[1].vertIndex = i+N*2+1;
		face[i+N*2-2].vert[2].vertIndex = i+N*5+1;
		face[i+N*2-2].vert[3].vertIndex = i+N*5;
		
	}
	// Draw top bot
	for (int i = 0; i < N-1; i++){
		face[i+N*3-3].nVerts = 4;
		face[i+N*3-3].vert = new VertexID[face[i+N*3-3].nVerts];
		face[i+N*3-3].vert[0].vertIndex = i;
		face[i+N*3-3].vert[1].vertIndex = i+1;
		face[i+N*3-3].vert[2].vertIndex = i+N*3+1;
		face[i+N*3-3].vert[3].vertIndex = i+N*3;
		
	}
	// Draw mid top
	for (int i = 0; i < N-1; i++){
		face[i+N*4-4].nVerts = 4;
		face[i+N*4-4].vert = new VertexID[face[i+N*4-4].nVerts];
		face[i+N*4-4].vert[0].vertIndex = i+N;
		face[i+N*4-4].vert[1].vertIndex = i+N+1;
		face[i+N*4-4].vert[2].vertIndex = i+N*4+1;
		face[i+N*4-4].vert[3].vertIndex = i+N*4;

	}
	// Draw mid sau
	for (int i = 0; i < N-1; i++){
		face[i+N*5-5].nVerts = 4;
		face[i+N*5-5].vert = new VertexID[face[i+N*5-5].nVerts];
		face[i+N*5-5].vert[0].vertIndex = i+N;
		face[i+N*5-5].vert[1].vertIndex = i+N+1;
		face[i+N*5-5].vert[2].vertIndex = N*7-2-i;
		face[i+N*5-5].vert[3].vertIndex = N*7-1-i;
		
	}
	// Draw mid truoc
	for (int i = 0; i < N-1; i++){
		face[i+N*6-6].nVerts = 4;
		face[i+N*6-6].vert = new VertexID[face[i+N*6-6].nVerts];
		face[i+N*6-6].vert[0].vertIndex = i+N*4;
		face[i+N*6-6].vert[1].vertIndex = i+N*4+1;
		face[i+N*6-6].vert[2].vertIndex = N*8-2-i;
		face[i+N*6-6].vert[3].vertIndex = N*8-1-i;
		
	}
	// Draw 4 coner
	face[N*7-7].nVerts = 4;
	face[N*7-7].vert = new VertexID[face[N*7-7].nVerts];
	face[N*7-7].vert[0].vertIndex = N*6;
	face[N*7-7].vert[1].vertIndex = N*8;
	face[N*7-7].vert[2].vertIndex = N*2;
	face[N*7-7].vert[3].vertIndex = 0;

	///
	face[N*7-6].nVerts = 4;
	face[N*7-6].vert = new VertexID[face[N*7-6].nVerts];
	face[N*7-6].vert[0].vertIndex = N*7;
	face[N*7-6].vert[1].vertIndex = N*8+2;
	face[N*7-6].vert[2].vertIndex = N*5;
	face[N*7-6].vert[3].vertIndex = N*3;

	/////
	face[N*7-5].nVerts = 4;
	face[N*7-5].vert = new VertexID[face[N*7-5].nVerts];
	face[N*7-5].vert[0].vertIndex = N*7-1;
	face[N*7-5].vert[1].vertIndex = N*8+1;
	face[N*7-5].vert[2].vertIndex = N*3-1;
	face[N*7-5].vert[3].vertIndex = N-1;

	///
	face[N*7-4].nVerts = 4;
	face[N*7-4].vert = new VertexID[face[N*7-4].nVerts];
	face[N*7-4].vert[0].vertIndex = N*8-1;
	face[N*7-4].vert[1].vertIndex = N*8+3;
	face[N*7-4].vert[2].vertIndex = N*6-1;
	face[N*7-4].vert[3].vertIndex = N*4-1;

	///// Draw 2 near
	face[N*7-3].nVerts = 4;
	face[N*7-3].vert = new VertexID[face[N*7-3].nVerts];
	face[N*7-3].vert[0].vertIndex = N*8;
	face[N*7-3].vert[1].vertIndex = N*8+2;
	face[N*7-3].vert[2].vertIndex = N*5;
	face[N*7-3].vert[3].vertIndex = N*2;

	///
	face[N*7-2].nVerts = 4;
	face[N*7-2].vert = new VertexID[face[N*7-2].nVerts];
	face[N*7-2].vert[0].vertIndex = N*8+1;
	face[N*7-2].vert[1].vertIndex = N*8+3;
	face[N*7-2].vert[2].vertIndex = N*6-1;
	face[N*7-2].vert[3].vertIndex = N*3-1;

}
#pragma endregion

Mesh	base1;
Mesh	base2;
Mesh	cylinder;
Mesh	ban;
Mesh	giado, giado1, giado2, giado3;
Mesh	rotor;
Mesh	thanhtruot, thanhtruot1;
Mesh	cocaulienket;
Mesh	chot;
Mesh	day;


#pragma region func
void myKeyboard(unsigned char key, int x, int y)
{
	float	fRInc;
	float	fAngle;
    switch(key)
    {
	case '1':
		base1.rotateY += base1RotateStep;
		if(base1.rotateY > 360)
			base1.rotateY -=360;
		break;
	case '2':	
		base1.rotateY -= base1RotateStep;
		if(base1.rotateY < 0)
			base1.rotateY +=360;
		break;
	case '3':
		cylinder.rotateY += cylinderRotateStep;
		if(cylinder.rotateY > 360)
			cylinder.rotateY -=360;
		break;
	case '4':	
		cylinder.rotateY -= cylinderRotateStep;
		if(cylinder.rotateY < 0)
			cylinder.rotateY +=360;
		break;
	case '5':
		cylinderOffset += cylinderTranslationStep;
		if(cylinderOffset > base2Height/2)
			cylinderOffset = base2Height/2;
		break;
	case '6':	
		cylinderOffset -= cylinderTranslationStep;
		if(cylinderOffset<0)
			cylinderOffset=0;
		break;
	case '7':
		rotor.rotateY += cylinderRotateStep;
		if(rotor.rotateY > 360)
			rotor.rotateY -=360;

		ovanOffset = 0.75*cos((rotor.rotateY-90)*PI/180);

		break;
	case '8':
		rotor.rotateY -= cylinderRotateStep;
		if(rotor.rotateY < 0)
			rotor.rotateY +=360;

		ovanOffset = 0.75*cos((rotor.rotateY-90)*PI/180);
		break;
	case '+':
		camera_dis += 0.5;
		if (camera_dis < 0){
			camera_dis == 0;
		}
		break;
	case '-':
		camera_dis -= 0.5;
		if (camera_dis < 0){
			camera_dis == 0;
		}
		break;
	case 'a':
		animation = !animation;
		break;
	case 'A':
		animation = !animation;
		break;
	case 'W':
		bWireFrame = !bWireFrame;
		break;
	case 'w':
		bWireFrame = !bWireFrame;
		break;
	case 'd':
		light = !light;
		break;
	case 'D':
		light = !light;
		break;

	}
    glutPostRedisplay();
}

void Animation(){
	if (animation){
		rotor.rotateY += 0.2;
		if(rotor.rotateY > 360)
			rotor.rotateY -=360;

		ovanOffset = 0.75*cos((rotor.rotateY-90)*PI/180);
	}
	glutPostRedisplay();
}

void specialKey(int key, int x, int y){
	switch(key){
	case GLUT_KEY_UP:
		camera_height += 0.5;
		break;
	case GLUT_KEY_DOWN:
		camera_height -= 0.5;
		break;
	case GLUT_KEY_RIGHT:
		camera_angle += 0.5;
		if (camera_angle > 360)
			camera_angle -= 360;
		break;
	case GLUT_KEY_LEFT:
		camera_angle -= 0.5;
		if (camera_angle < 0)
			camera_angle += 360;
		break;

	}
	glutPostRedisplay();
}

void setupMaterial(float ambient[], float diffuse[], float specular[], float shiness)
{
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shiness);
}


void drawAxis()
{
	glColor3f(0, 0, 1);
	glBegin(GL_LINES);
		glColor3f(1, 0, 0);
		glVertex3f(0, 0, 0);//x
		glVertex3f(4, 0, 0);

		glColor3f(0, 1, 0);
		glVertex3f(0, 0, 0);//y
		glVertex3f(0, 4, 0);

		glColor3f(0, 0, 1);
		glVertex3f(0, 0, 0);//z
		glVertex3f(0, 0, 4);
	glEnd();
}

void drawBase1()
{
	glPushMatrix();

		//glTranslated(0, base1Height/2.0, 0);
		
		glRotatef(base1.rotateY, 0, 1, 0);



		if(bWireFrame)
			base1.DrawWireframe();
		else
			base1.DrawColor();

	glPopMatrix();
}

void drawBase2()
{
	glPushMatrix();

		//glTranslated(0, base2Height/2.0+base1Height, 0);
		glTranslated(0, base1Height, 0);
		glRotatef(base1.rotateY, 0, 1, 0);

		if(bWireFrame)
			base2.DrawWireframe();
		else
			base2.DrawColor();

	glPopMatrix();
}

void drawCylinder()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		glTranslated(0, base1Height+cylinderOffset, 0);
		glRotatef(cylinder.rotateY+base1.rotateY , 0, 1, 0);

		if(bWireFrame)
			cylinder.DrawWireframe();
		else
			cylinder.DrawColor();

	glPopMatrix();
}

void drawBan()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		glTranslated(0, cylinderHeight+0.1+cylinderOffset, 0);
		glRotatef(cylinder.rotateY+base1.rotateY , 0, 1, 0);

		if(bWireFrame)
			ban.DrawWireframe();
		else
			ban.DrawColor();

	glPopMatrix();
}

void drawGiado()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		
		glTranslated(0, cylinderHeight+0.2+cylinderOffset + 0.1, 0);
		glRotatef(cylinder.rotateY+base1.rotateY , 0, 1, 0);
		

		if(bWireFrame)
			giado.DrawWireframe();
		else
			giado.DrawColor();

	glPopMatrix();
}

void drawGiado1()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		glTranslated(0, cylinderHeight+0.2+cylinderOffset + 0.2, 0);
		glRotatef(cylinder.rotateY+base1.rotateY , 0, 1, 0);

		glTranslatef(1.6, 0, 0);
		glRotatef(90, 0, 1, 0);

		if(bWireFrame)
			giado1.DrawWireframe();
		else
			giado1.DrawColor();

	glPopMatrix();
}


void drawGiado2()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		
		glTranslated(0, cylinderHeight+0.2+cylinderOffset + 0.1, 0);
		glRotatef(cylinder.rotateY+base1.rotateY , 0, 1, 0);
		

		if(bWireFrame)
			giado2.DrawWireframe();
		else
			giado2.DrawColor();

	glPopMatrix();
}

void drawGiado3()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		glTranslated(0, cylinderHeight+0.2+cylinderOffset + 0.2, 0);
		glRotatef(cylinder.rotateY+base1.rotateY , 0, 1, 0);

		glTranslatef(-1.6, 0, 0);
		glRotatef(-90, 0, 1, 0);

		if(bWireFrame)
			giado3.DrawWireframe();
		else
			giado3.DrawColor();

	glPopMatrix();
}

void drawRotor()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		glTranslated(0, cylinderHeight+0.2+cylinderOffset + 0.2, 0);
		glRotatef(cylinder.rotateY+base1.rotateY+rotor.rotateY , 0, 1, 0);


		if(bWireFrame)
			rotor.DrawWireframe();
		else
			rotor.DrawColor();

	glPopMatrix();
}

void drawThanhtruot()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		glTranslated(0, cylinderHeight+0.2+cylinderOffset + 0.2  + 0.4, 0);
		glRotatef(cylinder.rotateY+base1.rotateY , 0, 1, 0);

		glTranslatef(ovanOffset, 0, 0);

		glTranslatef(-0.3, 0, 0);
		glRotatef(90, 0, 0, 1);


		if(bWireFrame)
			thanhtruot.DrawWireframe();
		else
			thanhtruot.DrawColor();

	glPopMatrix();
}

void drawThanhtruot1()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		glTranslated(0, cylinderHeight+0.2+cylinderOffset + 0.2  + 0.4, 0);
		glRotatef(cylinder.rotateY+base1.rotateY , 0, 1, 0);

		glTranslatef(ovanOffset, 0, 0);

		glTranslatef(0.3, 0, 0);
		glRotatef(-90, 0, 0, 1);


		if(bWireFrame)
			thanhtruot1.DrawWireframe();
		else
			thanhtruot1.DrawColor();

	glPopMatrix();
}

void drawCocaulienket()
{
	glPushMatrix();

		//glTranslated(0, cylinderHeight/2.0+base1Height+cylinderOffset, 0);
		glTranslated(0, cylinderHeight+0.2+cylinderOffset + 0.2 + 0.2, 0);
		glRotatef(cylinder.rotateY+base1.rotateY , 0, 1, 0);

		glTranslatef(ovanOffset, 0, 0);
		glRotatef(90, 0, 1, 0);

		if(bWireFrame)
			cocaulienket.DrawWireframe();
		else
			cocaulienket.DrawColor();

	glPopMatrix();
}

void drawChot()
{
	glPushMatrix();

		glTranslated(0, cylinderHeight+0.2+cylinderOffset + 0.2 + 0.2, 0);
		glRotatef(cylinder.rotateY+base1.rotateY+rotor.rotateY , 0, 1, 0);

		glTranslatef(0, 0, 0.75);


		if(bWireFrame)
			chot.DrawWireframe();
		else
			chot.DrawColor();

	glPopMatrix();
}

void drawHang(float x){
	for(int i = 0; i <= 5; i++){
		glPushMatrix();
		if(x == 0){
			glTranslated(i*(2*sin(PI/3)), 0, 0);
		}
		else{
			glTranslated(i*(2*sin(PI/3)), 0, 0 + x*3);
		}
		day.DrawColor();

		glPopMatrix();
	
	}
	for(int i = 1; i <= 4; i++){
		glPushMatrix();
		
		if(x == 0){
			glTranslated(-i*(2*sin(PI/3)), 0, 0);
		}
		else{
			glTranslated(-i*(2*sin(PI/3)), 0, 0 + x*3);
		}

		day.DrawColor();

		glPopMatrix();
	
	}
}
void drawHangVer2(float x){
	glPushMatrix();
	glTranslated(sin(PI/3), 0, 1+cos(PI/3) + x*3);
	drawHang(0);
	glPopMatrix();
}
void drawDay()
{
	for (int i = -7; i < 7; i++){
		drawHang(i);
		drawHangVer2(i);
	}
	
}

void myDisplay()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(camera_angle, camera_height, camera_dis, 0, 1.0, 0, 0, 1, 0);


	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	
	glViewport(0, 0, screenWidth, screenHeight);

	
	//drawAxis();

	drawBase1();
	drawBase2();
	drawCylinder();
	drawBan();
	drawGiado();
	drawGiado1();
	drawGiado2();
	drawGiado3();
	drawRotor();
	drawThanhtruot();
	drawThanhtruot1();
	drawCocaulienket();
	drawChot();
	drawDay();

	

	//setupMaterial(lightAmbient2, lightDiffuse2, lightSpecular2, 0);
	
	glFlush();
    glutSwapBuffers();
}

void myInit()
{
	float	fHalfSize = 4;
	camera_angle = 3;
	camera_dis = 8;
	camera_height = 4;


	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	glFrontFace(GL_CCW);
	glEnable(GL_DEPTH_TEST);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-fHalfSize, fHalfSize, -fHalfSize, fHalfSize, -1000, 1000);
}

#pragma endregion

int _tmain(int argc, _TCHAR* argv[])
{
	glutInit(&argc, (char**)argv); //initialize the tool kit
	glutInitDisplayMode(GLUT_DOUBLE |GLUT_RGB | GLUT_DEPTH);//set the display mode
	glutInitWindowSize(screenWidth, screenHeight); //set window size
	glutInitWindowPosition(100, 100); // set window position on screen
	glutCreateWindow("Assignment - Dang Dinh Hien (1611089)"); // open the screen window

	cout << "1, 2: Rotate the base" << endl;
	cout << "3, 4: Rotate the cylinder" << endl;
	cout << "5, 6: Cylinder up/down" << endl;
	cout << "7, 8: Rotate the rotator" << endl;
	cout << "W, w: Switch between wireframe and solid mode" << endl;
	cout << "A, a: Turn on/off animation" << endl;
	cout << "D, d: Turn on/off the 2nd light source" << endl;
	cout << "+   : to increase camera distance." << endl;
	cout << "-   : to decrease camera distance." << endl;
	cout << "up arrow  : to increase camera height." << endl;
	cout << "down arrow: to decrease camera height." << endl;
	cout << "<-        : to rotate camera clockwise." << endl;
	cout << "->        : to rotate camera counterclockwise." << endl;

	base1.CreateTR(base1Radius, base1Height);
	base1.SetColor(0);

	base2.CreateTR(base2Radius, base2Height);
	base2.SetColor(0);

	cylinder.CreateTR(cylinderRadius, cylinderHeight+0.2);
	cylinder.SetColor(2);

	ban.CreateCuboid(3, 0.1, 1.5);
	ban.SetColor(1);

	giado.CreateDeGia(1.8); // x = 0.5, z = 0.2
	giado.SetColor(7);

	giado1.CreateBanNguyet(0.5, 0.4, 0.15, 0.7);
	giado1.SetColor(7);

	giado2.CreateDeGia(-1.8);
	giado2.SetColor(7);

	giado3.CreateBanNguyet(0.5, 0.4, 0.15, 0.7);
	giado3.SetColor(7);

	rotor.CreateTR(1.2, 0.2); // Du ra 0.1
	rotor.SetColor(14);

	thanhtruot.CreateTruVer2(0.2, 0.3, 0.15, 2.2, 0.1);
	thanhtruot.SetColor(3);

	thanhtruot1.CreateTruVer2(0.2, 0.3, 0.15, 2.2, 0.1);
	thanhtruot1.SetColor(3);

	cocaulienket.CreateOvan(0.3, 1.5, 0.14, 0.37); // y = 1.5
	cocaulienket.SetColor(3);

	chot.CreateTruVer2(0.14, 0.35, 0.1, 0, 0.1);
	chot.SetColor(14);

	day.CreateHinhThoi(1);

	

	myInit();

	
	glutKeyboardFunc(myKeyboard);
	glutSpecialFunc(specialKey);
    glutDisplayFunc(myDisplay);
	glutIdleFunc(Animation);
	  
	glutMainLoop();
	return 0;
}
