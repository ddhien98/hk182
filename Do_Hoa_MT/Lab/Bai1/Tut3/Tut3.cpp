#include <windows.h>
#include <glut.h>
#include <math.h>
#include <iostream>
#define PI 3.1415926



using namespace std;

int screenWidth = 500;
int screenHeight = 500;

void init()
{
    

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
 //   //glOrtho(0, screenWidth, 0, screenHeight, -1, 1);
 //   glOrtho(0, screenWidth, screenHeight, 0, -1, 1);
	//glClearColor(1, 1, 1, 1);
 //   glColor3f(1, 0, 0);
	gluOrtho2D(-4.0, 4.0, -4.0, 4.0);	// Dieu chinh do phan giai man hinh.
    glMatrixMode(GL_MODELVIEW);
    glClearColor(1.0, 1.0, 1.0,1.0);
    glColor3f(0.0,0.0,0.0);

}

void drawFigure1(){
	glBegin(GL_POLYGON);
		glVertex2f(0,0);
		glVertex2f(0,1);
		glVertex2f(1,1);
		glVertex2f(1,0);
	glEnd();
}

void drawFigure2(){
	glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(3, 1, 0); // Dich chuyen theo x, y, z
    glScalef(2/3.0, 1, 1); // Scale ti le theo x, y, z
    drawFigure1();
}

void drawFigure3(){
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1, 2, 0);
	glRotatef(45, 0,0,1);
	glScalef(1,0.5,1);
	drawFigure1();
}

void mydisplay(){
	glClear(GL_COLOR_BUFFER_BIT);
	drawFigure1();
	drawFigure2();
	drawFigure3();
	glFlush();
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(600, 600);
    glutInitWindowPosition(0, 0); // Vi tri cua so hien len
    glutCreateWindow("Tut 3");

    glutDisplayFunc(mydisplay);
    //glutMotionFunc(drawSquare);
	init();
	

    glutMainLoop();
}

